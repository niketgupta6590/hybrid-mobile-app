import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/createEmailPage.dart';
import 'package:leadcalls/menus/webview.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'objects/DeleteRecord.dart';
import 'objects/emails.dart';
import 'utils/ColorConstants.dart';
import 'utils/Constants.dart';

class EmailDetailPage extends StatefulWidget {
  EntryList emails;
  EmailDetailPage(this.emails);
  @override
  _EmailDetailPageState createState() => _EmailDetailPageState();
}

class _EmailDetailPageState extends State<EmailDetailPage> {




  String emailRecordId;
  @override
  void initState() {
    super.initState();
    print(widget.emails.toJson());
    print('-------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  }

  Future<void> deletedialog(context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Do you want to delete?"),
            actions: [
              FlatButton(
                child: Text("Yes"),
                onPressed: () {
                  _deleteItem();
                },
              ),
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        });
  }

  Future<void> _deleteItem() async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String restData =
        '{"session":"$userSession","module_name":"Emails","name_value_list":[{"name":"id","value":"$emailRecordId"},{"name":"deleted","value":"1"}]}';
    print("------------------------------------------->");
    print(emailRecordId);
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$restData"
    };
    print(restData);

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        DeleteRecord deleteRecord =
            DeleteRecord.fromJson(json.decode(response.toString()));

        String desc = "Delete successfully..";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);

        Navigator.of(context).pop();
        Navigator.pop(context,true);
        // Navigator.pushNamed(context, Home.id);

      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    emailRecordId = widget.emails.id;

    Widget body = new Container(
      margin: EdgeInsets.only(left: 0, right: 0, top: 8, bottom: 8),
      child: ListView(
        children: <Widget>[
          Container(
            height: 28.0,
            alignment: Alignment.center,
            color: ColorConstants.primaryColor,
            child: Text(
              "OVERVIEW",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Subject '),
            trailing: Text(
              widget.emails.nameValueList.name.value,

            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Full Name '),
            trailing: Text(
              widget.emails.nameValueList.parentName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('From '),
            trailing: Text(
              widget.emails.nameValueList.fromAddrName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('To '),
            trailing: Text(
              widget.emails.nameValueList.toAddrsNames.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Bcc '),
            trailing: Text(
              widget.emails.nameValueList.bccAddrsNames.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Cc '),
            trailing: Text(
              widget.emails.nameValueList.ccAddrsNames.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Status'),
            trailing: Text(
              widget.emails.nameValueList.status.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Short Description'),
            trailing: Text(
              widget.emails.nameValueList.description.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          Container(
            height: 28.0,
            alignment: Alignment.center,
            color: ColorConstants.primaryColor,
            child: Text(
              "INTERNAL",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Date Modified'),
            trailing: Text(
              widget.emails.nameValueList.dateModified.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Created By'),
            trailing: Text(
              widget.emails.nameValueList.createdByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Modified By'),
            trailing: Text(
              widget.emails.nameValueList.modifiedByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Assigned to'),
            trailing: Text(
              widget.emails.nameValueList.assignedUserName.value,
            ),
            onTap: () {},
          ),
        ],
      ),
    );



    return Scaffold(
      appBar: AppBar(
        title: Text('Email Details '),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
        /*bottom: TabBar(
            tabs: <Widget>[
              Tab(text: "DETAILS"),
              Tab(text: "HISTORY"),
              Tab(text: "RELATED"),
            ],
          ),*/
      ),
      body: body,
      /*TabBarView(
          children: <Widget>[
            body,
            History(),
            related,
          ],
        ),*/
    );
  }

  choiceAction(String choice) {
    if (choice == Constants.webView) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => webview(),
          )
      );    } else if (choice == Constants.share) {
      share(context);
    } else if (choice == Constants.edit) {

    } else if (choice == Constants.delete) {
      deletedialog(context);
    } else if (choice == Constants.scanQr) {
      ////   String cameraScabResult = await scanner.scan();

    } else if (choice == Constants.metadata) {}
  }

  // _sendData(){
  //
  //   String subject = widget.emails.nameValueList.name.value;
  //   String id = widget.emails.nameValueList.id.value;
  //   String desc = widget.emails.nameValueList.description.value;
  //   String from = widget.emails.nameValueList.fromAddrName.value;
  //   String createdby = widget.emails.nameValueList.createdByName.value;
  //   String modifyby = widget.emails.nameValueList.modifiedByName.value;
  //   String assignusername = widget.emails.nameValueList.assignedUserName.value;
  //   Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //         builder: (context) => MeetingEdit(firstnametext: subject, recordId:id,desc: desc,createdby: createdby,
  //           modifyby:modifyby,assusrname: assignusername,from:from),
  //       )
  //   );
  // }


  share(BuildContext context) {
    RenderBox box = context.findRenderObject();

    final String data =
        '${widget.emails.moduleName}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '
        'Full Name - ${widget.emails.nameValueList.parentName.value}\n '

        'Subject - ${widget.emails.nameValueList.subject.value}\n '
        'Status - ${widget.emails.nameValueList.status.value}\n '
        'Short Description - ${widget.emails.nameValueList.description.value}\n '
        'Date Modified - ${widget.emails.nameValueList.dateModified.value}\n'
        'Date Created - ${widget.emails.nameValueList.dateEntered.value}\n'
        'Created By - ${widget.emails.nameValueList.createdByName.value}\n'
        'Modified By - ${widget.emails.nameValueList.modifiedByName.value}\n'
        'Assigned to - ${widget.emails.nameValueList.assignedUserName.value}\n';

    print(data);

    Share.share(
      data,
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }


}
