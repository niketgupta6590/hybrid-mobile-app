import 'package:flutter/material.dart';
import 'package:leadcalls/utils/ColorConstants.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
              child: ListView(
        shrinkWrap: true,
        children: [
          Row(
            children: [
              // Image.asset("images/sale.png"),
              Text('Dunes\nSupport\nVersion:1.0.0.1'),
            ],
          ),
          Divider(
            thickness: 2.0,
            color: ColorConstants.primaryColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              Text('Help and Feedback'),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.report_problem),
                title: Text(('User Guide')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.report_problem),
                title: Text(('Video Tutorial')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.report_problem),
                title: Text(('Release Notes')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.event_note),
                title: Text(('FeedBack')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.announcement),
                title: Text(('Support')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.rate_review),
                title: Text(('Rate or Write Review')),
                onTap: () {},
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Divider(
            thickness: 2.0,
            color: ColorConstants.primaryColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              Text('Premium Features'),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.local_grocery_store),
                title: Text(('Buy Upgrade')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.report_problem),
                title: Text(('Corporate License Trial Request')),
                onTap: () {},
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Divider(
            thickness: 2.0,
            color: ColorConstants.primaryColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              Text('Legal'),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.surround_sound),
                title: Text(('OpenSource Licences')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.lock),
                title: Text(('Data Safety')),
                onTap: () {},
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.note_add),
                title: Text(('Terms of Services')),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {},
                ),
              ),
            ],
          ),
          Row(
            children: [
              ListTile(
                leading: Icon(Icons.person_outline),
                title: Text(('Privacy Policy')),
                onTap: () {},
              ),
            ],
          ),
        ],
      ))),
    );
  }
}
