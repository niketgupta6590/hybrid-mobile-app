import 'package:flutter/material.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'createEmailPage.dart';
import 'emailDetailPage.dart';
import 'objects/emails.dart';

class EmailsListPage extends StatefulWidget {
  Emails emails;

  EmailsListPage(this.emails);

  @override
  _EmailsListPageState createState() => _EmailsListPageState();
}

class _EmailsListPageState extends State<EmailsListPage> {
  @override
  Widget build(BuildContext context) {
    Widget body = ListView.builder(
      itemCount: widget.emails.entryList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () async {
            final isDeleted = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        EmailDetailPage(widget.emails.entryList[index])));
            if (isDeleted) {
              setState(() {
                widget.emails.entryList.removeAt(index);
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color: getColor(
                  widget.emails.entryList[index].nameValueList.status.value),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
//                        Icon(Icons.call),
//                        Icon(Icons.keyboard_arrow_right),
//                        Icon(Icons.person),
                        Text(
                          '${widget.emails.entryList[index].nameValueList.status.value}',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        Expanded(
                          child: Text(
                            '${widget.emails.entryList[index].nameValueList.dateEntered.value}',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                          '${widget.emails.entryList[index].nameValueList.name.value}'),
                      subtitle: Text(
                          '${widget.emails.entryList[index].nameValueList.description != null ? widget.emails.entryList[index].nameValueList.description.value : " ... "}'),
//                      trailing: Text(
//                          '${widget.leads.entryList[index].nameValueList.status.value}'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );

    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.add),
              onPressed: (){
                Navigator.pushNamed(context, CreateEmailPage.id);

              },
            )
          ],
          title: Text('Emails'),
        ),
        body:
         body,

    );
  }

  getColor(String status) {
    switch (status.toLowerCase().replaceAll(" ", "")) {
      case 'replied':
        return ColorConstants.assigned;
        break;
      case 'sent':
        return ColorConstants.open;
        break;
      case 'read':
        return ColorConstants.followUp;
        break;
      case 'unread':
        return ColorConstants.hot;
        break;
      case 'held':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      default:
        return Colors.white;
    }
  }
}
