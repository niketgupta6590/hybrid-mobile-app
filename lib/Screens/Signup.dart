import 'package:flutter/material.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Screens/ForgotPassword.dart';
import 'package:leadcalls/dashboardPage.dart';
import 'package:leadcalls/utils/ColorConstants.dart';

//author dhanashri shisode

class Signup extends StatefulWidget {
  static const id = 'signup';
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  TextEditingController nameController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailidController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();

  bool nameError = false;
  bool fullNameError = false;
  bool emailError = false;
  bool passwordError = false;
  bool confirmPasswordError = false;

  @override
  Widget build(BuildContext context) {
    Widget bottom =  Expanded(
      child:
      Align(
        alignment: FractionalOffset.bottomCenter,
        child:
        Padding(padding: EdgeInsets.only(bottom: 30.0),child:
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Image.asset(
                  'images/facebook.png',
                  width: 40.0,
                  height: 40.0,
                  fit: BoxFit.cover,
                ),
                onTap: (){
                  print('tap1');
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Image.asset(
                  'images/google.png',
                  width: 40.0,
                  height: 40.0,
                  fit: BoxFit.cover,
                ),
                onTap: (){print('tap1');},
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Image.asset(
                  'images/linkedin.png',
                  width: 40.0,
                  height: 40.0,
                  fit: BoxFit.cover,
                ),
                onTap: (){print('tap1');},
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Image.asset(
                  'images/twitter.png',
                  width: 40.0,
                  height: 40.0,
                  fit: BoxFit.cover,
                ),
                onTap: (){print('tap1');},
              ),
            ),

          ],
        ),
        ),
      ),
    );
    Widget body = Padding(
      padding: EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: TextField(
              controller: nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'User Name',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: TextField(
              controller: fullNameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Full Name',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: TextField(
              obscureText: true,
              controller: passwordController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: TextField(
              obscureText: true,
              controller: emailidController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Email Address'),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: TextField(
              obscureText: true,
              controller: confirmpasswordController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Confirm Password'),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: RaisedButton(
                textColor: Colors.white,
                color: ColorConstants.primaryColor,
                child: Text('SignUp'),
                onPressed: () {

                },
              )),
          Container(
              child: Row(
                children: <Widget>[
                  Text('Already have can account?'),
                  FlatButton(
                    textColor: ColorConstants.primaryColor,
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, LoginPage.id);
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              )
          ),

        ],
      ),

    );
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorConstants.primaryColor,
          title: Text(
            'SignUp',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w400, fontSize: 20),
          ),
        ),
        body: Column(
          children: [
            body,
            bottom
          ],
        )

    );
  }

  void signUp() {
    String userName = nameController.text;
    String fullName = fullNameController.text;
    String emailId = emailidController.text;
    String password = passwordController.text;
    String confirmPassword = confirmpasswordController.text;

    if (userName.isEmpty) {
      setState(() {
        nameError = true;
      });
      return;
    } else {
      nameError = false;
    }

    if (fullName.isEmpty) {
      setState(() {
        fullNameError = true;
      });
      return;
    } else {
      fullNameError = false;
    }

    if (emailId.isEmpty) {
      setState(() {
        emailError = true;
      });
      return;
    } else {
      emailError = false;
    }

    if (password.isEmpty) {
      setState(() {
        passwordError = true;
      });
      return;
    } else {
      passwordError = false;
    }

    if (confirmPassword.isEmpty) {
      setState(() {
        confirmPasswordError = true;
      });
      return;
    } else {
      confirmPasswordError = false;
    }
    _makePostRequest(userName, fullName, emailId, password, confirmPassword);
  }

  _makePostRequest(String userName, String fullName, String emailId,
      String password, String confirmPassword) async {
    // String url = 'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';
    //
    // String restData =
    //     '{"","module_name":"Calls","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","max_results":"50","deleted":"0","Favorites":false}';
    // Map<String, Object> body = {
    //   "input_type": "JSON",
    //   "response_type": "JSON",
    //   "method": "get_entry_list",
    //   "rest_data": "$restData"
    // };

    Map session = {
      "session": {
        "user_name": "$userName",
        "first_name": "$fullName",
        "last_name": "$emailId",
        //"UserType":"$",
        "new_password": "$password",
        "confirm_password": "$confirmPassword"
      },
    };
    // String userAuthString = '{"user_auth":{"user_name":"$email","password":"$password","encryption":"PLAIN"},"application":"MyRestAPI"}';

    // Map<String, Object> body = {
    //   "input_type": "JSON",
    //   "response_type": "JSON",
    //   "method": "set_entry",
    //   " rest_data" :"userInfo"
    // };
  }
}
