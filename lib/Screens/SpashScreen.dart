import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:leadcalls/LoginPage.dart';

class SplashScreen extends StatefulWidget {
  static const id = 'splashscreen';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  List<Slide> slides = new List();
  double height = 600.0;

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        heightImage: height,
        backgroundColor: Colors.blue,
        pathImage: "images/build.png",
        backgroundImageFit: BoxFit.cover,
        foregroundImageFit: BoxFit.cover,
      ),
    );
    slides.add(
      new Slide(
        heightImage: height,
        backgroundColor: Colors.blue,
        pathImage: "images/use.png",
        backgroundImageFit: BoxFit.cover,
        foregroundImageFit: BoxFit.cover,
      ),
    );
    slides.add(
      new Slide(
        backgroundColor: Colors.blue,
        heightImage: height,
        pathImage: "images/sale.png",
        backgroundImageFit: BoxFit.cover,
        foregroundImageFit: BoxFit.cover,
      ),
    );
    slides.add(
      new Slide(
        backgroundColor: Colors.blue,
        heightImage: height,
        pathImage: "images/agreement.png",
        backgroundImageFit: BoxFit.cover,
        foregroundImageFit: BoxFit.cover,
      ),
    );
  }

  void onDonePress() {
    Navigator.pushNamed(context, LoginPage.id);
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
    );

  }


  }

