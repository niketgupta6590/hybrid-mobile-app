import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:leadcalls/DomModels/MeetingDomResponse.dart';
import 'package:leadcalls/Models/LeadsResponseModule.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/Reused/LeadResponse.dart';
import 'package:leadcalls/Temps.dart';
import 'package:leadcalls/dashboardPage.dart';
import 'package:leadcalls/loginPage.dart';
import 'package:leadcalls/objects/meetingResponse.dart';
import 'package:leadcalls/utils/dropDownLists.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Screens/RelatedToSelection.dart';
import 'objects/meetings.dart';
import 'utils/Constants.dart';

class CreateMeetingPage extends StatefulWidget {
  static const id = 'meetings';
  mEntryList meeting;
  CreateMeetingPage(this.selectModule);
  String selectModule;
  @override
  _CreateMeetingPageState createState() => _CreateMeetingPageState();
}

class _CreateMeetingPageState extends State<CreateMeetingPage> {
  FlutterLocalNotificationsPlugin fltrNotification;

  String selectedRelatedTo;
  String selectedStatus1;
  String selectedPopupReminder;
  String selectedDurationHours;
  String selectedDurationMinutes;
  String formattedStartDate;
  String _selectedRemainder;

  String selectText = '';

  List<String> statusoptions = ['please wait..'];
  List<String> durationoptions = ['please wait..'];
  List<String> popup_options = ['please wait..'];
  List<String> remainder_options = ['please wait..'];
  List<String> related_to_options = ['please wait..'];
  List<String> status_options = ['please wait..'];
  List<String> duration_options = ['please wait..'];
  List<String> direction_options = ['please wait..'];

  var formatter = DateFormat('yyyy-MM-dd HH:mm a');
  var receivedDateFormatter = DateFormat('yyyy-MM-dd hh:mm:ss');
  DateTime startDate = DateTime.now();
  List<int> hours;

  bool showOverview = true;
  bool popupReminder = false;
  bool emailReminder = false;
  bool editInstance = false;
  bool Instance = false;

  TextEditingController _subjectController;
  TextEditingController descController;
  TextEditingController locController;
  TextEditingController createdByController;
  TextEditingController modifyByController;
  TextEditingController assignUserNameController;
  TextEditingController searchcontroller = new TextEditingController();

  LeadResponse list = new LeadResponse();
  Future<Map>
  _userData; // this with ??= of the next line is to prevent `getUserDetailsFromSharedPreference` to be called more than once
  Future<Map> get userData => _userData ??= list.getList(context);
  List<LeadsResponseModule> _searchResult = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    var androidInitialize = new AndroidInitializationSettings('icon');
    var iOSinitialize = new IOSInitializationSettings();
    var initilizationSettings =
    new InitializationSettings(androidInitialize, iOSinitialize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationSettings,
        onSelectNotification: notificationSelected);

    hours = List<int>.generate(60, (i) => i + 1).toList();

    _subjectController = TextEditingController();
    descController = TextEditingController();
    locController = TextEditingController();
    createdByController = TextEditingController();
    modifyByController = TextEditingController();
    assignUserNameController = TextEditingController();

    getdropdownValues();
  }

  @override
  Widget build(BuildContext context) {
    Future _showNotification() async {
      var androidDetails = new AndroidNotificationDetails(
          "channel Id ", "desi programmer", "this is my channel",
          importance: Importance.Max);
      var iosDetails = new IOSNotificationDetails();
      var generalNotificationDetails =
      new NotificationDetails(androidDetails, iosDetails);

      // await fltrNotification.show(
      //     0, "Reminder", "Schedule Notifications", generalNotificationDetails,payload:'Welcome to the Local Notification demo');

      var scheduledTime = DateTime.now().add(Duration(seconds: 5));
      fltrNotification.schedule(1, "Task", "scheduled Notification",
          scheduledTime, generalNotificationDetails);

      // var date = DateTime.parse(useDateTime);
      // var scheduledTime = date.subtract(Duration(minutes: 15));  // selected popup value write here
      // fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);
    }

    final Size screenSize = MediaQuery
        .of(context)
        .size;
    formattedStartDate = formatter.format(startDate);


    Widget createButton = Container(
      alignment: Alignment.bottomCenter,
      width: screenSize.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: RaisedButton(
              child: Text(
                'Cancel',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => {Navigator.pushNamed(context, DashboardPage.id)},
              color: Colors.grey,
            ),
          ),
          Expanded(
            child: RaisedButton(
              child: Text(
                'Save',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => _onCreateMeeting(),
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );


    return Scaffold(
      appBar: AppBar(
        title: Text('Create Meeting'),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          Container(
            child: SingleChildScrollView(
              padding: EdgeInsets.only(
                  top: 20.0, right: 20.0, left: 20.0, bottom: 50.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Flexible(
                          child: TextFormField(
                            controller: _subjectController,
                            decoration: InputDecoration(
                              labelText: 'Subject',
                              hintText: 'Subject',
                            ),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Related to'),
                    ],
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        value: selectedRelatedTo,
                        items: related_to_options.map((String title) {
                          return DropdownMenuItem(
                            value: title,
                            child: Text(title),
                          );
                        }).toList(),
                        onChanged: (changed) {
                          setState(() {
                            selectedRelatedTo = changed;
                          });
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text(selectText),
                      ),
                      Spacer(),
                      IconButton(
                          icon: Icon(Icons.attachment),
                          onPressed: () async {
                            final text = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        RelatedToSelection(selectedRelatedTo)));
                            setState(() {
                              selectText = text ?? '';
                            });
                          })
                    ],
                  ),
                  Row(
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: 20,
                          ),
                          Text('Start Date And Time '),
                          Container(
                            height: 10,
                          ),
                          GestureDetector(
                              onTap: () async {
                                await DatePicker.showDateTimePicker(
                                  context,
                                  minTime: DateTime(1900),
                                  currentTime: startDate,
                                  maxTime: DateTime(2200),
                                  onChanged: ((picked) {
                                    startDate = picked;
                                  }),
                                );
                                setState(() {});
                              },
                              child: Text(
                                '$formattedStartDate',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.0),
                              )),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Duration(Hours/Minutes)'),
                    ],
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        value: selectedDurationHours,
                        items: durationoptions.map((title) {
                          return DropdownMenuItem(
                            value: title,
                            child: Text(title),
                          );
                        }).toList(),
                        onChanged: (changed) {
                          setState(() {
                            selectedDurationHours = changed;
                          });
                        },
                      ),
                      DropdownButton(
                        value: selectedDurationMinutes,
                        items: durationoptions.map((title) {
                          return DropdownMenuItem(
                            value: title,
                            child: Text(title),
                          );
                        }).toList(),
                        onChanged: (changed) {
                          setState(() {
                            selectedDurationMinutes = changed;
                          });
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Status',
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        value: selectedStatus1,
                        items: statusoptions.map((title) {
                          return DropdownMenuItem(
                            value: title,
                            child: Text(title),
                          );
                        }).toList(),
                        onChanged: (changed) {
                          setState(() {
                            selectedStatus1 = changed;
                          });
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        'Remainder',
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        value: selectedPopupReminder,
                        items: DropDownLists.popuppreminder.map((title) {
                          return DropdownMenuItem(
                            value: title,
                            child: Text(title),
                          );
                        }).toList(),
                        onChanged: (changed) {
                          setState(() {
                            selectedPopupReminder = changed;
                          });
                        },
                      ),
                      // Text(' : '),
                      // DropdownButton(
                      //   value: selectedEmailReminder,
                      //   items: DropDownLists.emailreminder.map((title) {
                      //     return DropdownMenuItem(
                      //       value: title,
                      //       child: Text(title),
                      //     );
                      //   }).toList(),
                      //   onChanged: (changed) {
                      //     setState(() {
                      //       selectedDurationHours = changed;
                      //     });
                      //   },
                      // ),
                    ],
                  ),
                  Row(
                    children: [
                      Flexible(
                        child: TextFormField(
                          controller: descController,
                          decoration: InputDecoration(
                            labelText: 'Short Description',
                            hintText: 'Short Description',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Flexible(
                          child: TextFormField(
                            controller: locController,
                            decoration: InputDecoration(
                                labelText: 'Location', hintText: 'Location'),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Flexible(
                          child: TextFormField(
                            controller: createdByController,
                            decoration: InputDecoration(
                                labelText: 'Created by',
                                hintText: 'Created by'),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Flexible(
                          child: TextFormField(
                            controller: modifyByController,
                            decoration: InputDecoration(
                                labelText: 'Modified by',
                                hintText: 'Modified by'),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Flexible(
                          child: TextFormField(
                            controller: assignUserNameController,
                            decoration: InputDecoration(
                                labelText: 'Assigned by',
                                hintText: 'Assigned by'),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Add Invitees : ')],
                  ),
                  Row(
                    children: [

                      FlatButton(
                        child: Text('press'),
                        onPressed: () {
                          list.getList(context);
                          print(
                              '---------------getting dataaaaaaaaaaaaaaaaaaaaaaa');
                          print(list.getList(context));
                        },
                      )
                    ],
                  ),
                  Row(
                    children: [
                      ListTile(
                        leading: Icon(Icons.search),
                        title: TextField(
                          controller: searchcontroller,
                          decoration: InputDecoration(
                              hintText: 'Search', border: InputBorder.none),
                          onChanged: onSearchTextChanged,
                        ),
                        trailing: new IconButton(
                          icon: new Icon(Icons.cancel), onPressed: () {
                          searchcontroller.clear();
                          onSearchTextChanged('');
                        },),
                      ),
                    ],
                  )

                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            height: 60,
            width: screenSize.width,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: new RaisedButton(
                    child: new Text(
                      'Cancel',
                      style: new TextStyle(color: Colors.white),
                    ),
                    onPressed: () =>
                    {Navigator.pushNamed(context, DashboardPage.id)},
                    color: Colors.grey,
                  ),
                ),
                Expanded(
                  child: new RaisedButton(
                    child: new Text(
                      'Save',
                      style: new TextStyle(color: Colors.white),
                    ),
                    onPressed: () => _onCreateMeeting(),
                    color: Colors.blue,
                  ),
                ),
              ],
            ),
            margin: new EdgeInsets.only(top: 20.0),
          ),
        ],
      ),
    );
  }

  _onCreateMeeting() {
    String subjectValue = _subjectController.text;
    String location = locController.text;
    selectedRelatedTo;
    selectedDurationHours;
    selectedDurationMinutes;
    selectedStatus1;
    selectedPopupReminder;
    formattedStartDate;
    String shortDescriptionValue = descController.text;
    String createdby = createdByController.text;
    String modifyby = modifyByController.text;
    String assignusername = assignUserNameController.text;

    if (subjectValue.isEmpty && shortDescriptionValue.isEmpty) {
      setState(() {
        String desc = "Please enter subject";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
      return;
    }

    _onSchedule(
        location,
        subjectValue,
        selectedRelatedTo,
        selectedDurationHours,
        selectedDurationMinutes,
        selectedStatus1,
        selectedPopupReminder,
        shortDescriptionValue,
        formattedStartDate,
        createdby,
        modifyby,
        assignusername);
  }

  _onSchedule(String location,
      String subjectValue,
      String selectedRelatedTo,
      String selectedDurationHours,
      String selectedDurationMinutes,
      String selectedStatus1,
      String selectedpopupReminder,
      String shortDescriptionValue,
      String formattedStartDate,
      String createdby,
      String modifyby,
      String asssignusername) async {
    // set up POST request arguments
    String url = BASE_URL;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String meetingString =
        '{"session":"$userSession","module_name":"Meetings","name_value_list":[{"name":"assigned_user_id","value":"$userID"},{"name":"name","value":"$subjectValue"},{"name":"date_start","value":"$formattedStartDate"},{"name":"date_end","value":"$formattedStartDate"},{"name":"duration_minutes","value":"$selectedDurationMinutes"},{"name":"duration_hours","value":"$selectedDurationHours"},{"name":"location","value":"$location"},{"name":"parent_type","value":"$selectedRelatedTo"},{"name":"status","value":"$selectedStatus1"},{"name":"description","value":"$shortDescriptionValue"},{"name":"date_entered","value":"$formattedStartDate"},{"name":"date_modified","value":"$formattedStartDate"},{"name":"created_by_name","value":"$createdby"},{"name":"modified_by_name","value":"$modifyby"},{"name":"assigned_user_name","value":"$asssignusername"}]}';

    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$meetingString"
    };

    FormData formData = FormData.fromMap(body);

    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        MeetingResponse meetingresponse =
        MeetingResponse.fromJson(json.decode(response.toString()));

        String desc = "Meeting Scheduled Successfully";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = Completer();
    var contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  Future<void> getdropdownValues() async {
    //loading(context);
    // set up POST request arguments
    String url =
        'http://uat.ideadunes.com/projects/devs/testapi_giproperties/meeting_dom.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    int offset = 0;
    String restData =
        '{"session":"$userSession","module_name":"Meetings","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","select_fields":[],"link_name_to_fields_array":[],"max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sen
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        ///write a code from response
        MeetingDomResponse dropDownMeetingResponse =
        await MeetingDomResponse.fromJson(jsonDecode(response.toString()));
        setState(() {
          durationoptions =
              dropDownMeetingResponse.moduleFields.duration.options;
          selectedDurationHours = durationoptions[0];

          statusoptions = dropDownMeetingResponse.moduleFields.status.options;
          selectedStatus1 = statusoptions[0];

          popup_options =
              dropDownMeetingResponse.moduleFields.reminderTime.options;
          selectedPopupReminder = popup_options[0];

          remainder_options =
              dropDownMeetingResponse.moduleFields.emailReminderTime.options;
          _selectedRemainder = remainder_options[0];

          status_options = dropDownMeetingResponse.moduleFields.status.options;
          selectedStatus1 = status_options[0];

          related_to_options =
              dropDownMeetingResponse.moduleFields.parentName.options;
          selectedRelatedTo = related_to_options[0];

          duration_options =
              dropDownMeetingResponse.moduleFields.duration.options;
          selectedDurationMinutes = duration_options[0];
          selectedDurationHours = duration_options[0];
        });
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  Future notificationSelected(String payload) async {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: Text("Notification clicked $payload"),
          ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }
  }
}
