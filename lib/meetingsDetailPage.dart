import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:leadcalls/EditOption/MeetingEdit.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/LoginPage.dart';
import 'package:leadcalls/Reused/Notification.dart';
import 'package:leadcalls/callsListPage.dart';
import 'package:leadcalls/meetingsListPage.dart';
import 'package:leadcalls/menus/webview.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'DomModels/MeetingDomResponse.dart';
import 'Reused/Dialogs.dart';
import 'objects/DeleteRecord.dart';
import 'objects/meetings.dart';
import 'utils/ColorConstants.dart';
import 'utils/Constants.dart';

class MeetingsDetailsPage extends StatefulWidget {
  _MeetingsDetailsPageState createState() => _MeetingsDetailsPageState();

  EntryList meetings;
  MeetingsDetailsPage(this.meetings);
}

class _MeetingsDetailsPageState extends State<MeetingsDetailsPage> {

  FlutterLocalNotificationsPlugin  fltrNotification;


  String meetingRecordId;
  String _selectedPopup, _selectedRemainder;


  String selectedRelatedTo;
  String selectedStatus1;
  String selectedPopupReminder;
  String selectedDurationHours;
  String selectedDurationMinutes;
  String formattedStartDate;

  String selectText = '';

  List<String> popup_options = ['please wait..'];
  List<String> email_remainder_options = ['please wait..'];
  List<String> statusoptions = ['please wait..'];
  List<String> durationoptions = ['please wait..'];
  List<String> remainder_options = ['please wait..'];
  List<String> related_to_options = ['please wait..'];
  List<String> status_options = ['please wait..'];
  List<String> duration_options = ['please wait..'];
  List<String> direction_options = ['please wait..'];

  bool _isPopupSelected = true;
  bool _isEmailRemainderSelected = false;
  bool _isVisible = false;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();



    var androidInitialize = new AndroidInitializationSettings('icon');
    var iOSinitialize = new IOSInitializationSettings();
    var  initilizationSettings = new InitializationSettings(androidInitialize,iOSinitialize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationSettings,onSelectNotification: notificationSelected);

  }

  Future _showNotification()async {
    var androidDetails = new AndroidNotificationDetails(
        "channel Id ", "desi programmer", "this is my channel",
        importance: Importance.Max);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(androidDetails, iosDetails);

    // await fltrNotification.show(
    //     0, "Reminder", "Schedule Notifications", generalNotificationDetails,payload:'Welcome to the Local Notification demo');

    var scheduledTime = DateTime.now().add(Duration(seconds: 5));
    fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);

    // var date = DateTime.parse(useDateTime);
    // var scheduledTime = date.subtract(Duration(minutes: 15));  // selected popup value write here
    // fltrNotification.schedule(1, "Task", "scheduled Notification", scheduledTime, generalNotificationDetails);
  }

  Future<void> deletedialog(context) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Do you want to delete?"),
            actions: [
              FlatButton(
                child: Text("Yes"),
                onPressed: () {
                  _deleteItem();
                },
              ),
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        });
  }

  Future<void> _deleteItem() async {
    // set up POST request arguments
    String url =
        'http://hosting.ideadunes.com/hosting/giproperties.ae/service/v4_1/rest.php';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String restData =
        '{"session":"$userSession","module_name":"Meetings","name_value_list":[{"name":"id","value":"$meetingRecordId"},{"name":"deleted","value":"1"}]}';

    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$restData"
    };
    print(restData);

    FormData formData = FormData.fromMap(body);
    // make POST request
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
//    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        DeleteRecord deleteRecord =
            DeleteRecord.fromJson(json.decode(response.toString()));

        String desc = "Delete successfully..";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);

        //  Navigator.pushNamed(context, Home.id);
        Navigator.of(context).pop();
        Navigator.pop(context, true);
      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    meetingRecordId = widget.meetings.id;

    Widget body = new Container(
      margin: EdgeInsets.only(left: 0, right: 0, top: 8, bottom: 8),
      child: ListView(
        children: <Widget>[
          Container(
            height: 28.0,
            alignment: Alignment.center,
            color: ColorConstants.primaryColor,
            child: Text(
              "OVERVIEW",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Subject '),
            trailing: Text(
              widget.meetings.nameValueList.name.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Start Date '),
            trailing: Text(
              widget.meetings.nameValueList.dateStart.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Duration '),
            trailing: Text(
              widget.meetings.nameValueList.duration.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Remainder '),
            trailing: Icon(_isVisible ? Icons.expand_less : Icons.expand_more),
            onTap: showToast,
          ),
          Visibility(
            visible: _isVisible,
            child: SizedBox(
              height: 210.0,
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.all(8.0),
                      child:
                      Flexible(
                        child: Text(
                          "Actions : ",
                          style: TextStyle(fontSize: 20.0, color:Colors.black),
                        ),
                      )
                      ,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Switch(
                          value: _isPopupSelected,
                          onChanged: (value) {
                            setState(() {
                              _isPopupSelected = value;
                              _showNotification();
                              print(_isPopupSelected);
                            });
                          },
                          activeTrackColor: ColorConstants.primaryColor,
                          activeColor: ColorConstants.primaryColorDark,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text('Popup'),
                        Spacer(),
                        DropdownButton(
                          value: _selectedPopup,
                          items: popup_options.map((title) {
                            return DropdownMenuItem(
                              value: title,
                              child: Text(title),
                            );
                          }).toList(),
                          onChanged: (changed) {
                            setState(() {
                              _selectedPopup = changed;
                              // _showNotification();

                            });
                          },
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Switch(
                          value: _isEmailRemainderSelected,
                          onChanged: (value) {
                            setState(() {
                              _isEmailRemainderSelected = value;
                              _showNotification();
                              print(_isEmailRemainderSelected);
                            });
                          },
                          activeTrackColor: ColorConstants.primaryColor,
                          activeColor: ColorConstants.primaryColorDark,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text("Remainder"),
                        Spacer(),

                        DropdownButton(
                          value: _selectedRemainder,

                          items: popup_options.map((title) {
                            return DropdownMenuItem(
                              value: title,
                              child: Text(title),
                            );
                          }).toList(),
                          onChanged: (changed) {
                            setState(() {
                              _selectedRemainder = changed;
                              // _showNotification();
                            });
                          },
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 18.0),
                      child:
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FlatButton(onPressed: (){

                          },
                              child: Text("ADD ALL INVITEES")
                          ),
                          FlatButton(onPressed: (){

                          },
                              child: Text("REMOVE REMAINDER")
                          ),
                        ],
                      )
                      ,)

                  ],
                ),
              ),
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Location'),
            trailing: Text(
              widget.meetings.nameValueList.location.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Status'),
            trailing: Text(
              widget.meetings.nameValueList.status.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Short Description'),
            trailing: Text(
              widget.meetings.nameValueList.description.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          Container(
            height: 28.0,
            alignment: Alignment.center,
            color: ColorConstants.primaryColor,
            child: Text(
              "INTERNAL",
              textAlign: TextAlign.center,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Date Modified'),
            trailing: Text(
              widget.meetings.nameValueList.dateModified.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Date Created'),
            trailing: Text(
              widget.meetings.nameValueList.dateEntered.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Created By'),
            trailing: Text(
              widget.meetings.nameValueList.createdByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Modified By'),
            trailing: Text(
              widget.meetings.nameValueList.modifiedByName.value,
            ),
          ),
          Divider(
            thickness: 1.0,
            height: 1.0,
            color: Colors.black26,
          ),
          ListTile(
            leading: Text('Assigned To'),
            trailing: Text(
              widget.meetings.nameValueList.assignedUserName.value,
            ),
            onTap: () {},
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Meeting Details '),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: body,
    );
  }

  choiceAction(String choice) {
    if (choice == Constants.webView) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => webview(),
          ));
    } else if (choice == Constants.share) {
      share(context);
    } else if (choice == Constants.edit) {
      _sendData();
    } else if (choice == Constants.delete) {
      deletedialog(context);
    } else if (choice == Constants.scanQr) {
      ////   String cameraScabResult = await scanner.scan();
    } else if (choice == Constants.metadata) {}
  }

  _sendData() {
    String subject = widget.meetings.nameValueList.name.value;
    String id = widget.meetings.nameValueList.id.value;
    String desc = widget.meetings.nameValueList.description.value;
    String createdby = widget.meetings.nameValueList.createdByName.value;
    String modifyby = widget.meetings.nameValueList.modifiedByName.value;
    String assignusername =
        widget.meetings.nameValueList.assignedUserName.value;
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MeetingEdit(
            firstnametext: subject,
            recordId: id,
            desc: desc,
            createdby: createdby,
            modifyby: modifyby,
            assusrname: assignusername,
          ),
        ));
  }

  share(BuildContext context) {
    RenderBox box = context.findRenderObject();

    final String data =
        '${widget.meetings.moduleName}\n Subject - ${widget.meetings.nameValueList.name.value}\n '
        'Start Date - ${widget.meetings.nameValueList.dateEntered.value}\n '
        'End Date - ${widget.meetings.nameValueList.dateEnd.value}\n '
        'Duration - ${widget.meetings.nameValueList.duration.value}\n '
        'Status - ${widget.meetings.nameValueList.status.value}\n '
        'Date Modified - ${widget.meetings.nameValueList.dateModified.value}\n'
        'Date Created - ${widget.meetings.nameValueList.dateEntered.value}\n'
        'Created By - ${widget.meetings.nameValueList.createdByName.value}\n'
        'Modified By - ${widget.meetings.nameValueList.modifiedByName.value}\n'
        'Assigned to - ${widget.meetings.nameValueList.assignedUserName.value}\n';

    Share.share(
      data,
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }

  Future<void> getdropdownValues() async {

    String url = 'http://uat.ideadunes.com/projects/devs/testapi_giproperties/call_dom.php';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);
    int offset = 0;
    String restData = '{"session":"$userSession","module_name":"Calls","query":"assigned_user_id=\'$userID\'","order_by":"status","offset":"$offset","select_fields":[],"link_name_to_fields_array":[],"max_results":"50","deleted":"0","Favorites":false}';
    Map<String, dynamic> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "get_module_fields",
      "rest_data": "$restData"
    };
    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      return response; // continue
    }, onError: (DioError e) async {
      return e; //continue
    }));
    Response response = await dio.post(url, data: formData);
    // check the status code for the result
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());
      if (responseMap != null) {
        ///write a code from response
        MeetingDomResponse dropDownMeetingResponse =
        await MeetingDomResponse.fromJson(jsonDecode(response.toString()));
        setState(() {

          durationoptions =
              dropDownMeetingResponse.moduleFields.duration.options;
          selectedDurationHours = durationoptions[0];

          statusoptions = dropDownMeetingResponse.moduleFields.status.options;
          selectedStatus1 = statusoptions[0];

          popup_options =
              dropDownMeetingResponse.moduleFields.reminderTime.options;
          selectedPopupReminder = popup_options[0];

          remainder_options =
              dropDownMeetingResponse.moduleFields.emailReminderTime.options;
          _selectedRemainder = remainder_options[0];

          status_options = dropDownMeetingResponse.moduleFields.status.options;
          selectedStatus1 = status_options[0];

          related_to_options =
              dropDownMeetingResponse.moduleFields.parentName.options;
          selectedRelatedTo = related_to_options[0];

          duration_options =
              dropDownMeetingResponse.moduleFields.duration.options;
          selectedDurationMinutes = duration_options[0];
          selectedDurationHours = duration_options[0];
        });
      }
      else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);

        if (desc.contains("session ID is invalid")) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }


  Future  notificationSelected(String payload) async {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text("Notification clicked $payload"),
      ),
    );
  }
}
