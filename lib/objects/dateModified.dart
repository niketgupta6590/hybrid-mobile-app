class DateModifiedStruct {
  DateModified dateModified;

  DateModifiedStruct({this.dateModified});

  DateModifiedStruct.fromJson(Map<String, dynamic> json) {
    dateModified = json['date_modified'] != null
        ? new DateModified.fromJson(json['date_modified'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    return data;
  }
}

class DateModified {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  String len;

  DateModified(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  DateModified.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  EqSymbol eqSymbol;
  EqSymbol notEqual;
  EqSymbol greaterThan;
  EqSymbol lessThan;
  EqSymbol last7Days;
  EqSymbol next7Days;
  EqSymbol last30Days;
  EqSymbol next30Days;
  EqSymbol lastMonth;
  EqSymbol thisMonth;
  EqSymbol nextMonth;
  EqSymbol lastYear;
  EqSymbol thisYear;
  EqSymbol nextYear;
  EqSymbol between;

  Options(
      {this.eqSymbol,
        this.notEqual,
        this.greaterThan,
        this.lessThan,
        this.last7Days,
        this.next7Days,
        this.last30Days,
        this.next30Days,
        this.lastMonth,
        this.thisMonth,
        this.nextMonth,
        this.lastYear,
        this.thisYear,
        this.nextYear,
        this.between});

  Options.fromJson(Map<String, dynamic> json) {
    eqSymbol = json['eqSymbol'] != null
        ? new EqSymbol.fromJson(json['eqSymbol'])
        : null;
    notEqual = json['not_equal'] != null
        ? new EqSymbol.fromJson(json['not_equal'])
        : null;
    greaterThan = json['greater_than'] != null
        ? new EqSymbol.fromJson(json['greater_than'])
        : null;
    lessThan = json['less_than'] != null
        ? new EqSymbol.fromJson(json['less_than'])
        : null;
    last7Days = json['last_7_days'] != null
        ? new EqSymbol.fromJson(json['last_7_days'])
        : null;
    next7Days = json['next_7_days'] != null
        ? new EqSymbol.fromJson(json['next_7_days'])
        : null;
    last30Days = json['last_30_days'] != null
        ? new EqSymbol.fromJson(json['last_30_days'])
        : null;
    next30Days = json['next_30_days'] != null
        ? new EqSymbol.fromJson(json['next_30_days'])
        : null;
    lastMonth = json['last_month'] != null
        ? new EqSymbol.fromJson(json['last_month'])
        : null;
    thisMonth = json['this_month'] != null
        ? new EqSymbol.fromJson(json['this_month'])
        : null;
    nextMonth = json['next_month'] != null
        ? new EqSymbol.fromJson(json['next_month'])
        : null;
    lastYear = json['last_year'] != null
        ? new EqSymbol.fromJson(json['last_year'])
        : null;
    thisYear = json['this_year'] != null
        ? new EqSymbol.fromJson(json['this_year'])
        : null;
    nextYear = json['next_year'] != null
        ? new EqSymbol.fromJson(json['next_year'])
        : null;
    between =
    json['between'] != null ? new EqSymbol.fromJson(json['between']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.eqSymbol != null) {
      data['eqSymbol'] = this.eqSymbol.toJson();
    }
    if (this.notEqual != null) {
      data['not_equal'] = this.notEqual.toJson();
    }
    if (this.greaterThan != null) {
      data['greater_than'] = this.greaterThan.toJson();
    }
    if (this.lessThan != null) {
      data['less_than'] = this.lessThan.toJson();
    }
    if (this.last7Days != null) {
      data['last_7_days'] = this.last7Days.toJson();
    }
    if (this.next7Days != null) {
      data['next_7_days'] = this.next7Days.toJson();
    }
    if (this.last30Days != null) {
      data['last_30_days'] = this.last30Days.toJson();
    }
    if (this.next30Days != null) {
      data['next_30_days'] = this.next30Days.toJson();
    }
    if (this.lastMonth != null) {
      data['last_month'] = this.lastMonth.toJson();
    }
    if (this.thisMonth != null) {
      data['this_month'] = this.thisMonth.toJson();
    }
    if (this.nextMonth != null) {
      data['next_month'] = this.nextMonth.toJson();
    }
    if (this.lastYear != null) {
      data['last_year'] = this.lastYear.toJson();
    }
    if (this.thisYear != null) {
      data['this_year'] = this.thisYear.toJson();
    }
    if (this.nextYear != null) {
      data['next_year'] = this.nextYear.toJson();
    }
    if (this.between != null) {
      data['between'] = this.between.toJson();
    }
    return data;
  }
}

class EqSymbol {
  String name;
  String value;

  EqSymbol({this.name, this.value});

  EqSymbol.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

