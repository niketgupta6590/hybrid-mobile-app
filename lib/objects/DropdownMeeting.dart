class DropdownMeeting {
  String moduleName;
  String tableName;
  ModuleFields moduleFields;

  DropdownMeeting({this.moduleName, this.tableName, this.moduleFields});

  DropdownMeeting.fromJson(Map<String, dynamic> json) {
    moduleName = json['module_name'];
    tableName = json['table_name'];
    moduleFields = json['module_fields'] != null
        ? new ModuleFields.fromJson(json['module_fields'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module_name'] = this.moduleName;
    data['table_name'] = this.tableName;
    if (this.moduleFields != null) {
      data['module_fields'] = this.moduleFields.toJson();
    }
    return data;
  }
}

class ModuleFields {
  Status status;
  Duration duration;

  ModuleFields({this.status, this.duration});

  ModuleFields.fromJson(Map<String, dynamic> json) {
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    duration = json['duration'] != null
        ? new Duration.fromJson(json['duration'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.duration != null) {
      data['duration'] = this.duration.toJson();
    }
    return data;
  }
}

class Status {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  Status(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  Status.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class Duration {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;

  Duration(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  Duration.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}
