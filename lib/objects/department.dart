class DepartmentStruct {
  DepartmentsC departmentsC;

  DepartmentStruct({this.departmentsC});

  DepartmentStruct.fromJson(Map<String, dynamic> json) {
    departmentsC = json['departments_c'] != null
        ? new DepartmentsC.fromJson(json['departments_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.departmentsC != null) {
      data['departments_c'] = this.departmentsC.toJson();
    }
    return data;
  }
}

class DepartmentsC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  DepartmentsC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  DepartmentsC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class Options {
  SelectDepartments selectDepartments;
  SelectDepartments boardOfDirectors;
  SelectDepartments cXOs;
  SelectDepartments customerServicesDepartment;
  SelectDepartments department;
  SelectDepartments salesDepartment;
  SelectDepartments marketingDepartment;
  SelectDepartments humanResourcesDepartment;
  SelectDepartments projectManagement;
  SelectDepartments purchasingDepartment;
  SelectDepartments financeDepartment;
  SelectDepartments strategyPlanningDepartment;
  SelectDepartments operationsDepartment;
  SelectDepartments researchDevelopmentDepartment;
  SelectDepartments informationTechnologyDepartment;
  SelectDepartments inventoryManagementDepartment;
  SelectDepartments qualityAssuranceDepartment;
  SelectDepartments insuranceDepartment;
  SelectDepartments legalLicensesDepartment;
  SelectDepartments customerEngagementDepartment;
  SelectDepartments organizationalBehaviorDepartment;
  SelectDepartments businessDevelopmentDepartment;
  SelectDepartments engineeringDepartment;
  SelectDepartments supplyChainLogisticsDepartment;
  SelectDepartments startupLauncherDepartment;
  SelectDepartments ideaHandlingDepartment;
  SelectDepartments administrativeDepartment;
  SelectDepartments managementDepartment;
  SelectDepartments productionDepartment;
  SelectDepartments knowledgeBaseManagementDepartment;
  SelectDepartments securityComplianceDepartment;
  SelectDepartments socialMediaMarketingDepartment;
  SelectDepartments contentWritingDepartment;
  SelectDepartments developmentDepartment;
  SelectDepartments designingDepartment;
  SelectDepartments databaseManagementDepartment;
  SelectDepartments serverDepartment;
  SelectDepartments securityDepartment;
  SelectDepartments otherPleaseSpecify;

  Options(
      {this.selectDepartments,
        this.boardOfDirectors,
        this.cXOs,
        this.customerServicesDepartment,
        this.department,
        this.salesDepartment,
        this.marketingDepartment,
        this.humanResourcesDepartment,
        this.projectManagement,
        this.purchasingDepartment,
        this.financeDepartment,
        this.strategyPlanningDepartment,
        this.operationsDepartment,
        this.researchDevelopmentDepartment,
        this.informationTechnologyDepartment,
        this.inventoryManagementDepartment,
        this.qualityAssuranceDepartment,
        this.insuranceDepartment,
        this.legalLicensesDepartment,
        this.customerEngagementDepartment,
        this.organizationalBehaviorDepartment,
        this.businessDevelopmentDepartment,
        this.engineeringDepartment,
        this.supplyChainLogisticsDepartment,
        this.startupLauncherDepartment,
        this.ideaHandlingDepartment,
        this.administrativeDepartment,
        this.managementDepartment,
        this.productionDepartment,
        this.knowledgeBaseManagementDepartment,
        this.securityComplianceDepartment,
        this.socialMediaMarketingDepartment,
        this.contentWritingDepartment,
        this.developmentDepartment,
        this.designingDepartment,
        this.databaseManagementDepartment,
        this.serverDepartment,
        this.securityDepartment,
        this.otherPleaseSpecify});

  Options.fromJson(Map<String, dynamic> json) {
    selectDepartments = json['Select_Departments'] != null
        ? new SelectDepartments.fromJson(json['Select_Departments'])
        : null;
    boardOfDirectors = json['Board of Directors'] != null
        ? new SelectDepartments.fromJson(json['Board of Directors'])
        : null;
    cXOs = json['CXOs'] != null
        ? new SelectDepartments.fromJson(json['CXOs'])
        : null;
    customerServicesDepartment = json['Customer Services Department'] != null
        ? new SelectDepartments.fromJson(json['Customer Services Department'])
        : null;
    department = json['Department'] != null
        ? new SelectDepartments.fromJson(json['Department'])
        : null;
    salesDepartment = json['Sales Department'] != null
        ? new SelectDepartments.fromJson(json['Sales Department'])
        : null;
    marketingDepartment = json['Marketing Department'] != null
        ? new SelectDepartments.fromJson(json['Marketing Department'])
        : null;
    humanResourcesDepartment = json['Human Resources Department'] != null
        ? new SelectDepartments.fromJson(json['Human Resources Department'])
        : null;
    projectManagement = json['Project Management'] != null
        ? new SelectDepartments.fromJson(json['Project Management'])
        : null;
    purchasingDepartment = json['Purchasing Department'] != null
        ? new SelectDepartments.fromJson(json['Purchasing Department'])
        : null;
    financeDepartment = json['Finance Department'] != null
        ? new SelectDepartments.fromJson(json['Finance Department'])
        : null;
    strategyPlanningDepartment = json['Strategy/Planning Department'] != null
        ? new SelectDepartments.fromJson(json['Strategy/Planning Department'])
        : null;
    operationsDepartment = json['Operations Department'] != null
        ? new SelectDepartments.fromJson(json['Operations Department'])
        : null;
    researchDevelopmentDepartment =
    json['Research & Development Department'] != null
        ? new SelectDepartments.fromJson(
        json['Research & Development Department'])
        : null;
    informationTechnologyDepartment =
    json['Information Technology Department'] != null
        ? new SelectDepartments.fromJson(
        json['Information Technology Department'])
        : null;
    inventoryManagementDepartment =
    json['Inventory Management Department'] != null
        ? new SelectDepartments.fromJson(
        json['Inventory Management Department'])
        : null;
    qualityAssuranceDepartment = json['Quality Assurance Department'] != null
        ? new SelectDepartments.fromJson(json['Quality Assurance Department'])
        : null;
    insuranceDepartment = json['Insurance Department'] != null
        ? new SelectDepartments.fromJson(json['Insurance Department'])
        : null;
    legalLicensesDepartment = json['Legal/Licenses Department'] != null
        ? new SelectDepartments.fromJson(json['Legal/Licenses Department'])
        : null;
    customerEngagementDepartment = json['Customer Engagement Department'] !=
        null
        ? new SelectDepartments.fromJson(json['Customer Engagement Department'])
        : null;
    organizationalBehaviorDepartment =
    json['Organizational Behavior Department'] != null
        ? new SelectDepartments.fromJson(
        json['Organizational Behavior Department'])
        : null;
    businessDevelopmentDepartment =
    json['Business Development Department'] != null
        ? new SelectDepartments.fromJson(
        json['Business Development Department'])
        : null;
    engineeringDepartment = json['Engineering Department'] != null
        ? new SelectDepartments.fromJson(json['Engineering Department'])
        : null;
    supplyChainLogisticsDepartment =
    json['Supply Chain/Logistics Department'] != null
        ? new SelectDepartments.fromJson(
        json['Supply Chain/Logistics Department'])
        : null;
    startupLauncherDepartment = json['Startup Launcher Department'] != null
        ? new SelectDepartments.fromJson(json['Startup Launcher Department'])
        : null;
    ideaHandlingDepartment = json['Idea Handling Department'] != null
        ? new SelectDepartments.fromJson(json['Idea Handling Department'])
        : null;
    administrativeDepartment = json['Administrative Department'] != null
        ? new SelectDepartments.fromJson(json['Administrative Department'])
        : null;
    managementDepartment = json['Management Department'] != null
        ? new SelectDepartments.fromJson(json['Management Department'])
        : null;
    productionDepartment = json['Production Department'] != null
        ? new SelectDepartments.fromJson(json['Production Department'])
        : null;
    knowledgeBaseManagementDepartment =
    json['Knowledge Base Management Department'] != null
        ? new SelectDepartments.fromJson(
        json['Knowledge Base Management Department'])
        : null;
    securityComplianceDepartment = json['Security Compliance Department'] !=
        null
        ? new SelectDepartments.fromJson(json['Security Compliance Department'])
        : null;
    socialMediaMarketingDepartment =
    json['Social Media Marketing Department'] != null
        ? new SelectDepartments.fromJson(
        json['Social Media Marketing Department'])
        : null;
    contentWritingDepartment = json['Content Writing Department'] != null
        ? new SelectDepartments.fromJson(json['Content Writing Department'])
        : null;
    developmentDepartment = json['Development Department'] != null
        ? new SelectDepartments.fromJson(json['Development Department'])
        : null;
    designingDepartment = json['Designing Department'] != null
        ? new SelectDepartments.fromJson(json['Designing Department'])
        : null;
    databaseManagementDepartment = json['Database Management Department'] !=
        null
        ? new SelectDepartments.fromJson(json['Database Management Department'])
        : null;
    serverDepartment = json['Server Department'] != null
        ? new SelectDepartments.fromJson(json['Server Department'])
        : null;
    securityDepartment = json['Security Department'] != null
        ? new SelectDepartments.fromJson(json['Security Department'])
        : null;
    otherPleaseSpecify = json['Other Please Specify'] != null
        ? new SelectDepartments.fromJson(json['Other Please Specify'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.selectDepartments != null) {
      data['Select_Departments'] = this.selectDepartments.toJson();
    }
    if (this.boardOfDirectors != null) {
      data['Board of Directors'] = this.boardOfDirectors.toJson();
    }
    if (this.cXOs != null) {
      data['CXOs'] = this.cXOs.toJson();
    }
    if (this.customerServicesDepartment != null) {
      data['Customer Services Department'] =
          this.customerServicesDepartment.toJson();
    }
    if (this.department != null) {
      data['Department'] = this.department.toJson();
    }
    if (this.salesDepartment != null) {
      data['Sales Department'] = this.salesDepartment.toJson();
    }
    if (this.marketingDepartment != null) {
      data['Marketing Department'] = this.marketingDepartment.toJson();
    }
    if (this.humanResourcesDepartment != null) {
      data['Human Resources Department'] =
          this.humanResourcesDepartment.toJson();
    }
    if (this.projectManagement != null) {
      data['Project Management'] = this.projectManagement.toJson();
    }
    if (this.purchasingDepartment != null) {
      data['Purchasing Department'] = this.purchasingDepartment.toJson();
    }
    if (this.financeDepartment != null) {
      data['Finance Department'] = this.financeDepartment.toJson();
    }
    if (this.strategyPlanningDepartment != null) {
      data['Strategy/Planning Department'] =
          this.strategyPlanningDepartment.toJson();
    }
    if (this.operationsDepartment != null) {
      data['Operations Department'] = this.operationsDepartment.toJson();
    }
    if (this.researchDevelopmentDepartment != null) {
      data['Research & Development Department'] =
          this.researchDevelopmentDepartment.toJson();
    }
    if (this.informationTechnologyDepartment != null) {
      data['Information Technology Department'] =
          this.informationTechnologyDepartment.toJson();
    }
    if (this.inventoryManagementDepartment != null) {
      data['Inventory Management Department'] =
          this.inventoryManagementDepartment.toJson();
    }
    if (this.qualityAssuranceDepartment != null) {
      data['Quality Assurance Department'] =
          this.qualityAssuranceDepartment.toJson();
    }
    if (this.insuranceDepartment != null) {
      data['Insurance Department'] = this.insuranceDepartment.toJson();
    }
    if (this.legalLicensesDepartment != null) {
      data['Legal/Licenses Department'] = this.legalLicensesDepartment.toJson();
    }
    if (this.customerEngagementDepartment != null) {
      data['Customer Engagement Department'] =
          this.customerEngagementDepartment.toJson();
    }
    if (this.organizationalBehaviorDepartment != null) {
      data['Organizational Behavior Department'] =
          this.organizationalBehaviorDepartment.toJson();
    }
    if (this.businessDevelopmentDepartment != null) {
      data['Business Development Department'] =
          this.businessDevelopmentDepartment.toJson();
    }
    if (this.engineeringDepartment != null) {
      data['Engineering Department'] = this.engineeringDepartment.toJson();
    }
    if (this.supplyChainLogisticsDepartment != null) {
      data['Supply Chain/Logistics Department'] =
          this.supplyChainLogisticsDepartment.toJson();
    }
    if (this.startupLauncherDepartment != null) {
      data['Startup Launcher Department'] =
          this.startupLauncherDepartment.toJson();
    }
    if (this.ideaHandlingDepartment != null) {
      data['Idea Handling Department'] = this.ideaHandlingDepartment.toJson();
    }
    if (this.administrativeDepartment != null) {
      data['Administrative Department'] =
          this.administrativeDepartment.toJson();
    }
    if (this.managementDepartment != null) {
      data['Management Department'] = this.managementDepartment.toJson();
    }
    if (this.productionDepartment != null) {
      data['Production Department'] = this.productionDepartment.toJson();
    }
    if (this.knowledgeBaseManagementDepartment != null) {
      data['Knowledge Base Management Department'] =
          this.knowledgeBaseManagementDepartment.toJson();
    }
    if (this.securityComplianceDepartment != null) {
      data['Security Compliance Department'] =
          this.securityComplianceDepartment.toJson();
    }
    if (this.socialMediaMarketingDepartment != null) {
      data['Social Media Marketing Department'] =
          this.socialMediaMarketingDepartment.toJson();
    }
    if (this.contentWritingDepartment != null) {
      data['Content Writing Department'] =
          this.contentWritingDepartment.toJson();
    }
    if (this.developmentDepartment != null) {
      data['Development Department'] = this.developmentDepartment.toJson();
    }
    if (this.designingDepartment != null) {
      data['Designing Department'] = this.designingDepartment.toJson();
    }
    if (this.databaseManagementDepartment != null) {
      data['Database Management Department'] =
          this.databaseManagementDepartment.toJson();
    }
    if (this.serverDepartment != null) {
      data['Server Department'] = this.serverDepartment.toJson();
    }
    if (this.securityDepartment != null) {
      data['Security Department'] = this.securityDepartment.toJson();
    }
    if (this.otherPleaseSpecify != null) {
      data['Other Please Specify'] = this.otherPleaseSpecify.toJson();
    }
    return data;
  }
}

class SelectDepartments {
  String name;
  String value;

  SelectDepartments({this.name, this.value});

  SelectDepartments.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

