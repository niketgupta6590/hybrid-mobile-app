import 'package:leadcalls/objects/propertyStatus.dart';
import 'package:leadcalls/objects/subPropertyType.dart';

class DropDownLeadResponse {
  String moduleName;
  String tableName;
  ModuleFields moduleFields;

  DropDownLeadResponse({this.moduleName, this.tableName, this.moduleFields});

  DropDownLeadResponse.fromJson(Map<String, dynamic> json) {
    moduleName = json['module_name'];
    tableName = json['table_name'];
    moduleFields = json['module_fields'] != null
        ? new ModuleFields.fromJson(json['module_fields'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module_name'] = this.moduleName;
    data['table_name'] = this.tableName;
    if (this.moduleFields != null) {
      data['module_fields'] = this.moduleFields.toJson();
    }
    return data;
  }
}

class ModuleFields {
  Salutation salutation;
  // Title title;
  LeadSource leadSource;
  Status status;
  PropertyC propertyC;
  PropertyStatusC propertyStatusC;
  SubPropertyTypeC subPropertyTypeC;

  ModuleFields(
      {this.salutation,
        // this.title,
        this.leadSource,
        this.status,
        this.propertyC,
        this.propertyStatusC,
        this.subPropertyTypeC});

  ModuleFields.fromJson(Map<String, dynamic> json) {
    salutation = json['salutation'] != null
        ? new Salutation.fromJson(json['salutation'])
        : null;
    // title = json['title'] != null ? new Title.fromJson(json['title']) : null;
    leadSource = json['lead_source'] != null
        ? new LeadSource.fromJson(json['lead_source'])
        : null;
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    propertyC = json['property_c'] != null
        ? new PropertyC.fromJson(json['property_c'])
        : null;
    propertyStatusC = json['property_status_c'] != null
        ? new PropertyStatusC.fromJson(json['property_status_c'])
        : null;
    subPropertyTypeC = json['sub_property_type_c'] != null
        ? new SubPropertyTypeC.fromJson(json['sub_property_type_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.salutation != null) {
      data['salutation'] = this.salutation.toJson();
    }
    // if (this.title != null) {
    //   data['title'] = this.title.toJson();
    // }
    if (this.leadSource != null) {
      data['lead_source'] = this.leadSource.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.propertyC != null) {
      data['property_c'] = this.propertyC.toJson();
    }
    if (this.propertyStatusC != null) {
      data['property_status_c'] = this.propertyStatusC.toJson();
    }
    if (this.subPropertyTypeC != null) {
      data['sub_property_type_c'] = this.subPropertyTypeC.toJson();
    }
    return data;
  }
}

class Salutation {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;

  Salutation(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  Salutation.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

/*
class Title {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<Null> options;
  String relatedModule;
  bool calculated;
  String len;

  Title(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  Title.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    if (json['options'] != null) {
      options = new List<Null>();
      json['options'].forEach((v) {
        options.add(new Null.fromJson(v));
      });
    }
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.map((v) => v.toJson()).toList();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}
*/

class LeadSource {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;

  LeadSource(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  LeadSource.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Status {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  String len;
  String defaultValue;

  Status(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  Status.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class PropertyC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  PropertyC({this.name,
    this.type,
    this.group,
    this.idName,
    this.label,
    this.required,
    this.options,
    this.relatedModule,
    this.calculated,
    this.len,
    this.defaultValue});

  PropertyC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

  class PropertyStatusC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  PropertyStatusC(
  {this.name,
  this.type,
  this.group,
  this.idName,
  this.label,
  this.required,
  this.options,
  this.relatedModule,
  this.calculated,
  this.len,
  this.defaultValue});

  PropertyStatusC.fromJson(Map<String, dynamic> json) {
  name = json['name'];
  type = json['type'];
  group = json['group'];
  idName = json['id_name'];
  label = json['label'];
  required = json['required'];
  options = json['options'].cast<String>();
  relatedModule = json['related_module'];
  calculated = json['calculated'];
  len = json['len'];
  defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['name'] = this.name;
  data['type'] = this.type;
  data['group'] = this.group;
  data['id_name'] = this.idName;
  data['label'] = this.label;
  data['required'] = this.required;
  data['options'] = this.options;
  data['related_module'] = this.relatedModule;
  data['calculated'] = this.calculated;
  data['len'] = this.len;
  data['default_value'] = this.defaultValue;
  return data;
  }
}

class SubPropertyTypeC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  List<String> options;
  String relatedModule;
  bool calculated;
  int len;
  String defaultValue;

  SubPropertyTypeC({this.name,
    this.type,
    this.group,
    this.idName,
    this.label,
    this.required,
    this.options,
    this.relatedModule,
    this.calculated,
    this.len,
    this.defaultValue});

  SubPropertyTypeC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options = json['options'].cast<String>();
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    data['options'] = this.options;
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}
