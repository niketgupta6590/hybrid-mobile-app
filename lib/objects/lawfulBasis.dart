class LawfulBasisStruct {
  LawfulBasis lawfulBasis;

  LawfulBasisStruct({this.lawfulBasis});

  LawfulBasisStruct.fromJson(Map<String, dynamic> json) {
    lawfulBasis = json['lawful_basis'] != null
        ? new LawfulBasis.fromJson(json['lawful_basis'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lawfulBasis != null) {
      data['lawful_basis'] = this.lawfulBasis.toJson();
    }
    return data;
  }
}

class LawfulBasis {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  LawfulBasis(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  LawfulBasis.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Consent consent;
  Consent contract;
  Consent legalObligation;
  Consent protectionOfInterest;
  Consent publicInterest;
  Consent legitimateInterest;
  Consent withdrawn;

  Options(
      {this.consent,
        this.contract,
        this.legalObligation,
        this.protectionOfInterest,
        this.publicInterest,
        this.legitimateInterest,
        this.withdrawn});

  Options.fromJson(Map<String, dynamic> json) {
    consent =
    json['consent'] != null ? new Consent.fromJson(json['consent']) : null;
    contract = json['contract'] != null
        ? new Consent.fromJson(json['contract'])
        : null;
    legalObligation = json['legal_obligation'] != null
        ? new Consent.fromJson(json['legal_obligation'])
        : null;
    protectionOfInterest = json['protection_of_interest'] != null
        ? new Consent.fromJson(json['protection_of_interest'])
        : null;
    publicInterest = json['public_interest'] != null
        ? new Consent.fromJson(json['public_interest'])
        : null;
    legitimateInterest = json['legitimate_interest'] != null
        ? new Consent.fromJson(json['legitimate_interest'])
        : null;
    withdrawn = json['withdrawn'] != null
        ? new Consent.fromJson(json['withdrawn'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.consent != null) {
      data['consent'] = this.consent.toJson();
    }
    if (this.contract != null) {
      data['contract'] = this.contract.toJson();
    }
    if (this.legalObligation != null) {
      data['legal_obligation'] = this.legalObligation.toJson();
    }
    if (this.protectionOfInterest != null) {
      data['protection_of_interest'] = this.protectionOfInterest.toJson();
    }
    if (this.publicInterest != null) {
      data['public_interest'] = this.publicInterest.toJson();
    }
    if (this.legitimateInterest != null) {
      data['legitimate_interest'] = this.legitimateInterest.toJson();
    }
    if (this.withdrawn != null) {
      data['withdrawn'] = this.withdrawn.toJson();
    }
    return data;
  }
}

class Consent {
  String name;
  String value;

  Consent({this.name, this.value});

  Consent.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

