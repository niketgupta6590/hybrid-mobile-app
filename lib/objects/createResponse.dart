class CreateLeadResponse {
  String id;
  REntryList entryList;

  CreateLeadResponse({this.id, this.entryList});

  CreateLeadResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryList = json['entry_list'] != null
        ? new REntryList.fromJson(json['entry_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.toJson();
    }
    return data;
  }
}

class REntryList {
  ModifiedByName modifiedByName;
  ModifiedByName createdByName;
  ModifiedByName firstName;
  ModifiedByName lastName;
  ModifiedByName primaryAddressStreet;
  ModifiedByName phoneFax;
  ModifiedByName propertyC;
  ModifiedByName subPropertyTypeC;
  ModifiedByName propertyStatusC;
  ModifiedByName leadSource;
  ModifiedByName salutation;
  ModifiedByName status;
  ModifiedByName primaryAddressCity;
  ModifiedByName primaryAddressState;
  ModifiedByName altAddressPostalcode;
  ModifiedByName leadSourceDescription;
  ModifiedByName statusDescription;
  ModifiedByName referedBy;
  ModifiedByName campaignName;
  ModifiedByName opportunityAmount;
  ModifiedByName altAddressCity;
  ModifiedByName phoneWork;
  ModifiedByName altAddressState;
  ModifiedByName primaryAddressCountry;
  ModifiedByName altAddressStreet;

  REntryList(
      {this.modifiedByName,
        this.createdByName,
        this.firstName,
        this.lastName,
        this.primaryAddressStreet,
        this.phoneFax,
        this.propertyC,
        this.subPropertyTypeC,
        this.propertyStatusC,
        this.leadSource,
        this.salutation,
        this.status,
        this.primaryAddressCity,
        this.primaryAddressState,
        this.altAddressPostalcode,
        this.leadSourceDescription,
        this.statusDescription,
        this.referedBy,
        this.campaignName,
        this.opportunityAmount,
        this.altAddressCity,
        this.phoneWork,
        this.altAddressState,
        this.primaryAddressCountry,
        this.altAddressStreet});

  REntryList.fromJson(Map<String, dynamic> json) {
    modifiedByName = json['modified_by_name'] != null
        ? new ModifiedByName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new ModifiedByName.fromJson(json['created_by_name'])
        : null;
    firstName = json['first_name'] != null
        ? new ModifiedByName.fromJson(json['first_name'])
        : null;
    lastName = json['last_name'] != null
        ? new ModifiedByName.fromJson(json['last_name'])
        : null;
    primaryAddressStreet = json['primary_address_street'] != null
        ? new ModifiedByName.fromJson(json['primary_address_street'])
        : null;
    phoneFax = json['phone_fax'] != null
        ? new ModifiedByName.fromJson(json['phone_fax'])
        : null;
    propertyC = json['property_c'] != null
        ? new ModifiedByName.fromJson(json['property_c'])
        : null;
    subPropertyTypeC = json['sub_property_type_c'] != null
        ? new ModifiedByName.fromJson(json['sub_property_type_c'])
        : null;
    propertyStatusC = json['property_status_c'] != null
        ? new ModifiedByName.fromJson(json['property_status_c'])
        : null;
    leadSource = json['lead_source'] != null
        ? new ModifiedByName.fromJson(json['lead_source'])
        : null;
    salutation = json['salutation'] != null
        ? new ModifiedByName.fromJson(json['salutation'])
        : null;
    status = json['status'] != null
        ? new ModifiedByName.fromJson(json['status'])
        : null;
    primaryAddressCity = json['primary_address_city'] != null
        ? new ModifiedByName.fromJson(json['primary_address_city'])
        : null;
    primaryAddressState = json['primary_address_state'] != null
        ? new ModifiedByName.fromJson(json['primary_address_state'])
        : null;
    altAddressPostalcode = json['alt_address_postalcode'] != null
        ? new ModifiedByName.fromJson(json['alt_address_postalcode'])
        : null;
    leadSourceDescription = json['lead_source_description'] != null
        ? new ModifiedByName.fromJson(json['lead_source_description'])
        : null;
    statusDescription = json['status_description'] != null
        ? new ModifiedByName.fromJson(json['status_description'])
        : null;
    referedBy = json['refered_by'] != null
        ? new ModifiedByName.fromJson(json['refered_by'])
        : null;
    campaignName = json['campaign_name'] != null
        ? new ModifiedByName.fromJson(json['campaign_name'])
        : null;
    opportunityAmount = json['opportunity_amount'] != null
        ? new ModifiedByName.fromJson(json['opportunity_amount'])
        : null;
    altAddressCity = json['alt_address_city'] != null
        ? new ModifiedByName.fromJson(json['alt_address_city'])
        : null;
    phoneWork = json['phone_work'] != null
        ? new ModifiedByName.fromJson(json['phone_work'])
        : null;
    altAddressState = json['alt_address_state'] != null
        ? new ModifiedByName.fromJson(json['alt_address_state'])
        : null;
    primaryAddressCountry = json['primary_address_country'] != null
        ? new ModifiedByName.fromJson(json['primary_address_country'])
        : null;
    altAddressStreet = json['alt_address_street'] != null
        ? new ModifiedByName.fromJson(json['alt_address_street'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.firstName != null) {
      data['first_name'] = this.firstName.toJson();
    }
    if (this.lastName != null) {
      data['last_name'] = this.lastName.toJson();
    }
    if (this.primaryAddressStreet != null) {
      data['primary_address_street'] = this.primaryAddressStreet.toJson();
    }
    if (this.phoneFax != null) {
      data['phone_fax'] = this.phoneFax.toJson();
    }
    if (this.propertyC != null) {
      data['property_c'] = this.propertyC.toJson();
    }
    if (this.subPropertyTypeC != null) {
      data['sub_property_type_c'] = this.subPropertyTypeC.toJson();
    }
    if (this.propertyStatusC != null) {
      data['property_status_c'] = this.propertyStatusC.toJson();
    }
    if (this.leadSource != null) {
      data['lead_source'] = this.leadSource.toJson();
    }
    if (this.salutation != null) {
      data['salutation'] = this.salutation.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.primaryAddressCity != null) {
      data['primary_address_city'] = this.primaryAddressCity.toJson();
    }
    if (this.primaryAddressState != null) {
      data['primary_address_state'] = this.primaryAddressState.toJson();
    }
    if (this.altAddressPostalcode != null) {
      data['alt_address_postalcode'] = this.altAddressPostalcode.toJson();
    }
    if (this.leadSourceDescription != null) {
      data['lead_source_description'] = this.leadSourceDescription.toJson();
    }
    if (this.statusDescription != null) {
      data['status_description'] = this.statusDescription.toJson();
    }
    if (this.referedBy != null) {
      data['refered_by'] = this.referedBy.toJson();
    }
    if (this.campaignName != null) {
      data['campaign_name'] = this.campaignName.toJson();
    }
    if (this.opportunityAmount != null) {
      data['opportunity_amount'] = this.opportunityAmount.toJson();
    }
    if (this.altAddressCity != null) {
      data['alt_address_city'] = this.altAddressCity.toJson();
    }
    if (this.phoneWork != null) {
      data['phone_work'] = this.phoneWork.toJson();
    }
    if (this.altAddressState != null) {
      data['alt_address_state'] = this.altAddressState.toJson();
    }
    if (this.primaryAddressCountry != null) {
      data['primary_address_country'] = this.primaryAddressCountry.toJson();
    }
    if (this.altAddressStreet != null) {
      data['alt_address_street'] = this.altAddressStreet.toJson();
    }
    return data;
  }
}

class ModifiedByName {
  String name;
  String value;

  ModifiedByName({this.name, this.value});

  ModifiedByName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
