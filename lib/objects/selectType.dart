class SelectTypeStruct {
  SelectTypeC selectTypeC;

  SelectTypeStruct({this.selectTypeC});

  SelectTypeStruct.fromJson(Map<String, dynamic> json) {
    selectTypeC = json['select_type_c'] != null
        ? new SelectTypeC.fromJson(json['select_type_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.selectTypeC != null) {
      data['select_type_c'] = this.selectTypeC.toJson();
    }
    return data;
  }
}

class SelectTypeC {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  SelectTypeC(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  SelectTypeC.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Individual individual;
  Individual company;

  Options({this.individual, this.company});

  Options.fromJson(Map<String, dynamic> json) {
    individual = json['Individual'] != null
        ? new Individual.fromJson(json['Individual'])
        : null;
    company = json['Company'] != null
        ? new Individual.fromJson(json['Company'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.individual != null) {
      data['Individual'] = this.individual.toJson();
    }
    if (this.company != null) {
      data['Company'] = this.company.toJson();
    }
    return data;
  }
}

class Individual {
  String name;
  String value;

  Individual({this.name, this.value});

  Individual.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

