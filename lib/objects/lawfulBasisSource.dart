class LawfulBasisSourceStruct {
  LawfulBasisSource lawfulBasisSource;

  LawfulBasisSourceStruct({this.lawfulBasisSource});

  LawfulBasisSourceStruct.fromJson(Map<String, dynamic> json) {
    lawfulBasisSource = json['lawful_basis_source'] != null
        ? new LawfulBasisSource.fromJson(json['lawful_basis_source'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lawfulBasisSource != null) {
      data['lawful_basis_source'] = this.lawfulBasisSource.toJson();
    }
    return data;
  }
}

class LawfulBasisSource {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  int len;

  LawfulBasisSource(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  LawfulBasisSource.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Website website;
  Website phone;
  Website givenToUser;
  Website email;
  Website thirdParty;

  Options(
      {this.website,
        this.phone,
        this.givenToUser,
        this.email,
        this.thirdParty});

  Options.fromJson(Map<String, dynamic> json) {
    website =
    json['website'] != null ? new Website.fromJson(json['website']) : null;
    phone = json['phone'] != null ? new Website.fromJson(json['phone']) : null;
    givenToUser = json['given_to_user'] != null
        ? new Website.fromJson(json['given_to_user'])
        : null;
    email = json['email'] != null ? new Website.fromJson(json['email']) : null;
    thirdParty = json['third_party'] != null
        ? new Website.fromJson(json['third_party'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.website != null) {
      data['website'] = this.website.toJson();
    }
    if (this.phone != null) {
      data['phone'] = this.phone.toJson();
    }
    if (this.givenToUser != null) {
      data['given_to_user'] = this.givenToUser.toJson();
    }
    if (this.email != null) {
      data['email'] = this.email.toJson();
    }
    if (this.thirdParty != null) {
      data['third_party'] = this.thirdParty.toJson();
    }
    return data;
  }
}

class Website {
  String name;
  String value;

  Website({this.name, this.value});

  Website.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

