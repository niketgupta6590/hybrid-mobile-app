class EventAcceptStatusStruct {
  EventAcceptStatus eventAcceptStatus;

  EventAcceptStatusStruct({this.eventAcceptStatus});

  EventAcceptStatusStruct.fromJson(Map<String, dynamic> json) {
    eventAcceptStatus = json['event_accept_status'] != null
        ? new EventAcceptStatus.fromJson(json['event_accept_status'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.eventAcceptStatus != null) {
      data['event_accept_status'] = this.eventAcceptStatus.toJson();
    }
    return data;
  }
}

class EventAcceptStatus {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  String len;

  EventAcceptStatus(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len});

  EventAcceptStatus.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    return data;
  }
}

class Options {
  Accepted accepted;
  Accepted declined;
  Accepted noResponse;

  Options({this.accepted, this.declined, this.noResponse});

  Options.fromJson(Map<String, dynamic> json) {
    accepted = json['Accepted'] != null
        ? new Accepted.fromJson(json['Accepted'])
        : null;
    declined = json['Declined'] != null
        ? new Accepted.fromJson(json['Declined'])
        : null;
    noResponse = json['No Response'] != null
        ? new Accepted.fromJson(json['No Response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.accepted != null) {
      data['Accepted'] = this.accepted.toJson();
    }
    if (this.declined != null) {
      data['Declined'] = this.declined.toJson();
    }
    if (this.noResponse != null) {
      data['No Response'] = this.noResponse.toJson();
    }
    return data;
  }
}

class Accepted {
  String name;
  String value;

  Accepted({this.name, this.value});

  Accepted.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

