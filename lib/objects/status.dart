class StatusStruct {
  Status status;

  StatusStruct({this.status});

  StatusStruct.fromJson(Map<String, dynamic> json) {
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    return data;
  }
}

class Status {
  String name;
  String type;
  String group;
  String idName;
  String label;
  int required;
  Options options;
  String relatedModule;
  bool calculated;
  String len;
  String defaultValue;

  Status(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.label,
        this.required,
        this.options,
        this.relatedModule,
        this.calculated,
        this.len,
        this.defaultValue});

  Status.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    label = json['label'];
    required = json['required'];
    options =
    json['options'] != null ? new Options.fromJson(json['options']) : null;
    relatedModule = json['related_module'];
    calculated = json['calculated'];
    len = json['len'];
    defaultValue = json['default_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['label'] = this.label;
    data['required'] = this.required;
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['related_module'] = this.relatedModule;
    data['calculated'] = this.calculated;
    data['len'] = this.len;
    data['default_value'] = this.defaultValue;
    return data;
  }
}

class Options {
  Assigned assigned;
  Assigned open;
  Assigned followUp;
  Assigned hot;
  Assigned cold;
  Assigned news;
  Assigned recycled;
  Assigned sold;
  Assigned dead;

  Options(
      {this.assigned,
        this.open,
        this.followUp,
        this.hot,
        this.cold,
        this.news,
        this.recycled,
        this.sold,
        this.dead});

  Options.fromJson(Map<String, dynamic> json) {
    assigned = json['Assigned'] != null
        ? new Assigned.fromJson(json['Assigned'])
        : null;
    open = json['Open'] != null ? new Assigned.fromJson(json['Open']) : null;
    followUp = json['FollowUp'] != null
        ? new Assigned.fromJson(json['FollowUp'])
        : null;
    hot = json['Hot'] != null ? new Assigned.fromJson(json['Hot']) : null;
    cold = json['Cold'] != null ? new Assigned.fromJson(json['Cold']) : null;
    news = json['New'] != null ? new Assigned.fromJson(json['New']) : null;
    recycled = json['Recycled'] != null
        ? new Assigned.fromJson(json['Recycled'])
        : null;
    sold = json['sold'] != null ? new Assigned.fromJson(json['sold']) : null;
    dead = json['Dead'] != null ? new Assigned.fromJson(json['Dead']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assigned != null) {
      data['Assigned'] = this.assigned.toJson();
    }
    if (this.open != null) {
      data['Open'] = this.open.toJson();
    }
    if (this.followUp != null) {
      data['FollowUp'] = this.followUp.toJson();
    }
    if (this.hot != null) {
      data['Hot'] = this.hot.toJson();
    }
    if (this.cold != null) {
      data['Cold'] = this.cold.toJson();
    }
    if (this.news != null) {
      data['New'] = this.news.toJson();
    }
    if (this.recycled != null) {
      data['Recycled'] = this.recycled.toJson();
    }
    if (this.sold != null) {
      data['sold'] = this.sold.toJson();
    }
    if (this.dead != null) {
      data['Dead'] = this.dead.toJson();
    }
    return data;
  }
}

class Assigned {
  String name;
  String value;

  Assigned({this.name, this.value});

  Assigned.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}

