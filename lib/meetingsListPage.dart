import 'package:flutter/material.dart';
import 'createMeetingPage.dart';
import 'meetingsDetailPage.dart';
import 'objects/meetings.dart';
import 'utils/ColorConstants.dart';

class MeetingsListPage extends StatefulWidget {
  static const id = 'meetinglist';

  Meetings meetings;

  MeetingsListPage(this.meetings);

  @override
  _MeetingsListPageState createState() => _MeetingsListPageState();
}

class _MeetingsListPageState extends State<MeetingsListPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = ListView.builder(
      itemCount: widget.meetings.entryList.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: ()  async{
            final isDeleted = await
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        MeetingsDetailsPage(widget.meetings.entryList[index])));
            if (isDeleted) {
              setState(() {
                widget.meetings.entryList.removeAt(index);
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              color: getColor(
                  widget.meetings.entryList[index].nameValueList.status.value),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        // Text(
                        //   '${widget.meetings.entryList[index].nameValueList.status.value} / ${widget.meetings.entryList[index].nameValueList.status.value}',
                        //   style: TextStyle(fontWeight: FontWeight.w500),
                        // ),
                        Expanded(
                          child: Text(
                            '${widget.meetings.entryList[index].nameValueList.dateEntered.value}',
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                          '${widget.meetings.entryList[index].nameValueList.name.value}'),
                      // subtitle: Text(
                      //     '${widget.meetings.entryList[index].nameValueList.description != null ? widget.meetings.entryList[index].nameValueList.description.value : " ... "}'),
                      trailing: Text(
                          '${widget.meetings.entryList[index].nameValueList.status.value}'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.add),
              onPressed: (){

              Navigator.pushNamed(context, CreateMeetingPage.id);


              },
            )
          ],
          title: Text('Meetings'),
        ),
        body: body);
  }

  getColor(String status) {
    switch (status.toLowerCase().replaceAll(" ", "")) {
      case 'planned':
        return ColorConstants.assigned;
        break;
      case 'open':
        return ColorConstants.open;
        break;
      case 'followup':
        return ColorConstants.followUp;
        break;
      case 'hot':
        return ColorConstants.hot;
        break;
      case 'held':
        return ColorConstants.cold;
        break;
      case 'new':
        return ColorConstants.newStatus;
        break;
      case 'recycled':
        return ColorConstants.recycled;
        break;
      case 'sold':
        return ColorConstants.soldSatus;
        break;
      case 'dead':
        return ColorConstants.dead;
        break;
      default:
        return Colors.white;
    }
  }
}
