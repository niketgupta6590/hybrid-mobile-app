import 'package:flutter/material.dart';
import 'package:leadcalls/CreateModuleEntry/CreateContact.dart';
import 'package:leadcalls/Data.dart';
import 'package:leadcalls/Home.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/Screens/Calender.dart';
import 'package:leadcalls/Screens/ForgotPassword.dart';
import 'package:leadcalls/Screens/LoginCRMURL.dart';
import 'package:leadcalls/Screens/ModuleList.dart';
import 'package:leadcalls/Screens/NotesList.dart';
import 'package:leadcalls/Screens/Profile.dart';
import 'package:leadcalls/Screens/Signup.dart';
import 'package:leadcalls/Screens/SpashScreen.dart';
import 'package:leadcalls/Temps.dart';
import 'package:leadcalls/createCallPage.dart';
import 'package:leadcalls/createEmailPage.dart';
import 'package:leadcalls/createLeadPage.dart';
import 'package:leadcalls/createMeetingPage.dart';
import 'package:leadcalls/utils/ColorConstants.dart';
import 'package:leadcalls/utils/Constants.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'CreateModuleEntry/CreateNote.dart';
import 'Models/LeadsResponseModule.dart';
import 'Screens/ThemeModel.dart';
import 'dashboardPage.dart';
import 'LoginPage.dart';

void main() => runApp(
      ChangeNotifierProvider(
        create: (context) => ThemeModel(),
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  EntryList details;
  String selectmodule;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Dunes Support',
      theme: Provider.of<ThemeModel>(context).currentTheme,
      home: MyHomePage(),
      routes: {
        LoginPage.id: (context) => LoginPage(),
        DashboardPage.id: (context) => DashboardPage(),
        SplashScreen.id:(context) => SplashScreen(),
        Home.id: (context) => Home(),
        Calender.id:(context) => Calender(),
        CreateEmailPage.id:(context) => CreateEmailPage(selectmodule),
        CreateCallPage.id:(context) => CreateCallPage(selectmodule),
        CreateLeadPage.id:(context) => CreateLeadPage(selectmodule),
        CreateMeetingPage.id:(context) => CreateMeetingPage(selectmodule),
        CreateContact.id:(context) => CreateContact(selectmodule),
        ModuleList.id: (context) => ModuleList(),
        Edit_profile.id:(context) => Edit_profile(),
        CreateNotes.id:(context) => CreateNotes(),
        Signup.id:(context) => Signup(),
        ForgotPassword.id:(context)=>ForgotPassword(),
        CRMLogin.id:(context)=>CRMLogin(),
        test.id:(context)=>test(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
  static void setAppFontFamily(BuildContext context, String _selectedFontFamily) {
    // ignore: deprecated_member_use
    _MyHomePageState state = context.ancestorStateOfType(TypeMatcher<_MyHomePageState>());

  }
}

class _MyHomePageState extends State<MyHomePage> {
  // String _fontFamily = GoogleFonts.allan.toString();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      checkLoginStatus();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          flex: 8,
          child: Stack(
            children: [
              Image.asset(
                'images/splash.jpg',
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
              SafeArea(
                child: Container(
                  margin: EdgeInsets.only(left: 10, top: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: ColorConstants.primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Text('Dunes',
                            style: TextStyle(fontSize: 40, color: cWhite)),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: ColorConstants.primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Text('Support',
                            style: TextStyle(fontSize: 40, color: cWhite)),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      Expanded(child:
          Container(
            color: cWhite,
            width: double.infinity,
            padding: const EdgeInsets.all(20.0),
            child: Image.asset('images/powered.png'),
          ),),

      ],
    ));
  }

  Future<void> checkLoginStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool loginStatus = prefs.getBool(Constants.loginStatusPrefKey) == null
        ? false
        : prefs.getBool(Constants.loginStatusPrefKey);
    if (loginStatus != null && loginStatus) {
      tUserSession = prefs.getString(Constants.id);
      tUserId = prefs.getString(Constants.userId);
      Navigator.pushReplacementNamed(context, DashboardPage.id);
    } else {
      Navigator.pushReplacementNamed(context, SplashScreen.id);
    }
  }
}
