import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:leadcalls/Res/Colors.dart';
import 'package:leadcalls/Temps.dart';
import 'objects/emailResponse.dart';
import 'objects/emails.dart';
import 'utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CreateEmailPage extends StatefulWidget {
  static const id = 'creteemail';
  EntryList emails;
  CreateEmailPage(this.seelectModule);
  String seelectModule;
  @override
  _CreateEmailPageState createState() => _CreateEmailPageState();
}

class _CreateEmailPageState extends State<CreateEmailPage> {
  bool bcc = false;
  bool cc = false;

  String selectedStatus1;
  List<String> statusoptions = ['please wait..'];



  final _fromController = TextEditingController();
  final _toController = TextEditingController();
  final _ccController = TextEditingController();
  final _bccController = TextEditingController();
  final _subjectController = TextEditingController();
  final _bodyController = TextEditingController();

  // Timer _timer;
  //
  // _CreateEmailPageState() {
  //   _timer = new Timer(const Duration(milliseconds: 400), () {
  //     setState(() {
  //       _fromController.text = widget.emails.nameValueList.fromAddrName.value;
  //     });
  //   });
  // }

  bool _isVisible = false;
  initState() {
    super.initState();

    // _timer.cancel();
  }

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Email'),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: _fromController.text != null
            ? ListView(physics: ClampingScrollPhysics(), children: [
                Row(
                  children: [
                    Padding(padding: EdgeInsets.all(12.0),
                    child:
                        Row(
                          children: [


                    Text(
                      'Status',
                      style: TextStyle(fontSize: 12),
                    ),
                    DropdownButton(
                      value: selectedStatus1,
                      items: statusoptions.map((title) {
                        return DropdownMenuItem(
                          value: title,
                          child: Text(title),
                        );
                      }).toList(),
                      onChanged: (changed) {
                        setState(() {
                          selectedStatus1 = changed;
                        });
                      },
                    ),
                          ],
                        ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Text(
                        'From',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: _fromController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Text(
                        'To',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: _toController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                          _isVisible ? Icons.expand_less : Icons.expand_more),
                      onPressed: showToast,
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: 0.3,
                  color: cBlack.withOpacity(0.4),
                ),
                Visibility(
                  visible: _isVisible,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 12.0),
                        child: Text(
                          'Cc',
                          style: TextStyle(fontSize: 18.0, color: Colors.grey),
                        ),
                      ),
                      Flexible(
                        child: TextFormField(
                          controller: _ccController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none),
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: _isVisible,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 12.0),
                        child: Text(
                          'Bcc',
                          style: TextStyle(fontSize: 18.0, color: Colors.grey),
                        ),
                      ),
                      Flexible(
                        child: TextFormField(
                          controller: _bccController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Text(
                        'Subject',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: _subjectController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: 0.3,
                  color: cBlack.withOpacity(0.4),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Text(
                        'Compose email',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: _bodyController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: 0.3,
                  color: cBlack.withOpacity(0.4),
                ),
                Container(
                  height: 12,
                ),
                RaisedButton(
                  child: Text('Send'),
                  color: Colors.blue,
                  textColor: Colors.white,
                  onPressed: () => onCreateEmail(),
                )
              ])
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Please wait while we get your contacts...')
                  ],
                ),
              ),
      ),
    );
  }

  onCreateEmail() {
    String toValue = _toController.text;
    String bccValue = _bccController.text;
    String ccValue = _ccController.text;
    String subjectValue = _subjectController.text;
    String bodyValue = _bodyController.text;

    if (toValue.isEmpty && subjectValue.isEmpty && bodyValue.isEmpty) {
      setState(() {
        String desc = "Please enter details";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
      return;
    }

    _onSend(toValue, bccValue, ccValue, subjectValue, bodyValue);
  }

  _onSend(String toValue, String bccValue, String ccValue, String subjectValue,
      String bodyValue) async {
    // set up POST request arguments
    String url = BASE_URL;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userSession = prefs.getString(Constants.id);
    String userID = prefs.getString(Constants.userId);

    String emailString =
        '{"session":"$userSession","module_name":"Emails", "name_value_list":[{"name":"assigned_user_id","value":"$userID"},{"name":"to_addrs_names","value":"$toValue"},{"name":"bcc_addrs_names","value":"$bccValue"},{"name":"cc_addrs_names","value":"$ccValue"},{"name":"name","value":"$subjectValue"},{"name":"description","value":"$bodyValue"}]}';

    print(emailString);

    Map<String, Object> body = {
      "input_type": "JSON",
      "response_type": "JSON",
      "method": "set_entry",
      "rest_data": "$emailString"
    };

    FormData formData = FormData.fromMap(body);
    Dio dio = new Dio();

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // Do something before request is sent
      print(options.data);
      print(options);
      return options; //continue
    }, onResponse: (Response response) async {
      // Do something with response data
      return response; // continue
    }, onError: (DioError e) async {
      // Do something with response error
      return e; //continue
    }));

    Response response = await dio.post(url, data: formData);
    int statusCode = response.statusCode;

    if (statusCode == 200) {
      Map responseMap = json.decode(response.toString());

      if (responseMap != null) {
        EmailResponse emailresponse = EmailResponse.fromJson(json.decode(response.toString()));

        String desc = "Email send";
        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);



      } else {
        String desc = responseMap.containsKey('description')
            ? responseMap['description']
            : "Something went wrong. Try again.";

        Fluttertoast.showToast(
            msg: "$desc",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  String generateMd5(String input) {
    /////    return md5.convert(utf8.encode(input)).toString();
  }

  Future<dynamic> readResponse(HttpClientResponse response) {
    var completer = new Completer();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }
}
