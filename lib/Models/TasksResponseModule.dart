class TasksModuleResponse {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<TEntryList> entryList;
  // List<Null> relationshipList;

  TasksModuleResponse(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
        // this.relationshipList
      });

  TasksModuleResponse.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<TEntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new TEntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class TEntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  TEntryList({this.id, this.moduleName, this.nameValueList});

  TEntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName status;
  AssignedUserName dateDueFlag;
  AssignedUserName dateDue;
  AssignedUserName timeDue;
  AssignedUserName dateStartFlag;
  AssignedUserName dateStart;
  AssignedUserName parentType;
  AssignedUserName parentName;
  AssignedUserName parentId;
  AssignedUserName contactId;
  AssignedUserName contactName;
  AssignedUserName contactPhone;
  AssignedUserName contactEmail;
  AssignedUserName priority;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.status,
        this.dateDueFlag,
        this.dateDue,
        this.timeDue,
        this.dateStartFlag,
        this.dateStart,
        this.parentType,
        this.parentName,
        this.parentId,
        this.contactId,
        this.contactName,
        this.contactPhone,
        this.contactEmail,
        this.priority});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    status = json['status'] != null
        ? new AssignedUserName.fromJson(json['status'])
        : null;
    dateDueFlag = json['date_due_flag'] != null
        ? new AssignedUserName.fromJson(json['date_due_flag'])
        : null;
    dateDue = json['date_due'] != null
        ? new AssignedUserName.fromJson(json['date_due'])
        : null;
    timeDue = json['time_due'] != null
        ? new AssignedUserName.fromJson(json['time_due'])
        : null;
    dateStartFlag = json['date_start_flag'] != null
        ? new AssignedUserName.fromJson(json['date_start_flag'])
        : null;
    dateStart = json['date_start'] != null
        ? new AssignedUserName.fromJson(json['date_start'])
        : null;
    parentType = json['parent_type'] != null
        ? new AssignedUserName.fromJson(json['parent_type'])
        : null;
    parentName = json['parent_name'] != null
        ? new AssignedUserName.fromJson(json['parent_name'])
        : null;
    parentId = json['parent_id'] != null
        ? new AssignedUserName.fromJson(json['parent_id'])
        : null;
    contactId = json['contact_id'] != null
        ? new AssignedUserName.fromJson(json['contact_id'])
        : null;
    contactName = json['contact_name'] != null
        ? new AssignedUserName.fromJson(json['contact_name'])
        : null;
    contactPhone = json['contact_phone'] != null
        ? new AssignedUserName.fromJson(json['contact_phone'])
        : null;
    contactEmail = json['contact_email'] != null
        ? new AssignedUserName.fromJson(json['contact_email'])
        : null;
    priority = json['priority'] != null
        ? new AssignedUserName.fromJson(json['priority'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.dateDueFlag != null) {
      data['date_due_flag'] = this.dateDueFlag.toJson();
    }
    if (this.dateDue != null) {
      data['date_due'] = this.dateDue.toJson();
    }
    if (this.timeDue != null) {
      data['time_due'] = this.timeDue.toJson();
    }
    if (this.dateStartFlag != null) {
      data['date_start_flag'] = this.dateStartFlag.toJson();
    }
    if (this.dateStart != null) {
      data['date_start'] = this.dateStart.toJson();
    }
    if (this.parentType != null) {
      data['parent_type'] = this.parentType.toJson();
    }
    if (this.parentName != null) {
      data['parent_name'] = this.parentName.toJson();
    }
    if (this.parentId != null) {
      data['parent_id'] = this.parentId.toJson();
    }
    if (this.contactId != null) {
      data['contact_id'] = this.contactId.toJson();
    }
    if (this.contactName != null) {
      data['contact_name'] = this.contactName.toJson();
    }
    if (this.contactPhone != null) {
      data['contact_phone'] = this.contactPhone.toJson();
    }
    if (this.contactEmail != null) {
      data['contact_email'] = this.contactEmail.toJson();
    }
    if (this.priority != null) {
      data['priority'] = this.priority.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
