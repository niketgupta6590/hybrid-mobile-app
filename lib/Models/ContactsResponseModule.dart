class ContactsResponseModule {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<CEntryList> entryList;
  // List<Null> relationshipList;

  ContactsResponseModule(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
        // this.relationshipList
      });

  ContactsResponseModule.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<CEntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new CEntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class CEntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  CEntryList({this.id, this.moduleName, this.nameValueList});

  CEntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName salutation;
  AssignedUserName firstName;
  AssignedUserName lastName;
  AssignedUserName fullName;
  AssignedUserName title;
  AssignedUserName photo;
  AssignedUserName department;
  AssignedUserName doNotCall;
  AssignedUserName phoneHome;
  AssignedUserName email;
  AssignedUserName phoneMobile;
  AssignedUserName phoneWork;
  AssignedUserName phoneOther;
  AssignedUserName phoneFax;
  AssignedUserName email1;
  AssignedUserName email2;
  AssignedUserName invalidEmail;
  AssignedUserName emailOptOut;
  AssignedUserName lawfulBasis;
  AssignedUserName dateReviewed;
  AssignedUserName lawfulBasisSource;
  AssignedUserName primaryAddressStreet;
  AssignedUserName primaryAddressStreet2;
  AssignedUserName primaryAddressStreet3;
  AssignedUserName primaryAddressCity;
  AssignedUserName primaryAddressState;
  AssignedUserName primaryAddressPostalcode;
  AssignedUserName primaryAddressCountry;
  AssignedUserName altAddressStreet;
  AssignedUserName altAddressStreet2;
  AssignedUserName altAddressStreet3;
  AssignedUserName altAddressCity;
  AssignedUserName altAddressState;
  AssignedUserName altAddressPostalcode;
  AssignedUserName altAddressCountry;
  AssignedUserName assistant;
  AssignedUserName assistantPhone;
  AssignedUserName emailAddressesNonPrimary;
  AssignedUserName emailAndName1;
  AssignedUserName leadSource;
  AssignedUserName opportunityRoleFields;
  AssignedUserName opportunityRoleId;
  AssignedUserName opportunityRole;
  AssignedUserName reportsToId;
  AssignedUserName reportToName;
  AssignedUserName birthdate;
  AssignedUserName campaignId;
  AssignedUserName campaignName;
  AssignedUserName cAcceptStatusFields;
  AssignedUserName mAcceptStatusFields;
  AssignedUserName acceptStatusId;
  AssignedUserName acceptStatusName;
  AssignedUserName syncContact;
  AssignedUserName eInviteStatusFields;
  AssignedUserName eventStatusName;
  AssignedUserName eventInviteId;
  AssignedUserName eAcceptStatusFields;
  AssignedUserName eventAcceptStatus;
  AssignedUserName eventStatusId;
  AssignedUserName joomlaAccountId;
  AssignedUserName portalAccountDisabled;
  AssignedUserName joomlaAccountAccess;
  AssignedUserName portalUserType;
  AssignedUserName isYourOwnAccountC;
  AssignedUserName jjwgMapsLngC;
  AssignedUserName jjwgMapsGeocodeStatusC;
  AssignedUserName profilePictureC;
  AssignedUserName birthDateC;
  AssignedUserName jjwgMapsAddressC;
  AssignedUserName jjwgMapsLatC;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.salutation,
        this.firstName,
        this.lastName,
        this.fullName,
        this.title,
        this.photo,
        this.department,
        this.doNotCall,
        this.phoneHome,
        this.email,
        this.phoneMobile,
        this.phoneWork,
        this.phoneOther,
        this.phoneFax,
        this.email1,
        this.email2,
        this.invalidEmail,
        this.emailOptOut,
        this.lawfulBasis,
        this.dateReviewed,
        this.lawfulBasisSource,
        this.primaryAddressStreet,
        this.primaryAddressStreet2,
        this.primaryAddressStreet3,
        this.primaryAddressCity,
        this.primaryAddressState,
        this.primaryAddressPostalcode,
        this.primaryAddressCountry,
        this.altAddressStreet,
        this.altAddressStreet2,
        this.altAddressStreet3,
        this.altAddressCity,
        this.altAddressState,
        this.altAddressPostalcode,
        this.altAddressCountry,
        this.assistant,
        this.assistantPhone,
        this.emailAddressesNonPrimary,
        this.emailAndName1,
        this.leadSource,
        this.opportunityRoleFields,
        this.opportunityRoleId,
        this.opportunityRole,
        this.reportsToId,
        this.reportToName,
        this.birthdate,
        this.campaignId,
        this.campaignName,
        this.cAcceptStatusFields,
        this.mAcceptStatusFields,
        this.acceptStatusId,
        this.acceptStatusName,
        this.syncContact,
        this.eInviteStatusFields,
        this.eventStatusName,
        this.eventInviteId,
        this.eAcceptStatusFields,
        this.eventAcceptStatus,
        this.eventStatusId,
        this.joomlaAccountId,
        this.portalAccountDisabled,
        this.joomlaAccountAccess,
        this.portalUserType,
        this.isYourOwnAccountC,
        this.jjwgMapsLngC,
        this.jjwgMapsGeocodeStatusC,
        this.profilePictureC,
        this.birthDateC,
        this.jjwgMapsAddressC,
        this.jjwgMapsLatC});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    salutation = json['salutation'] != null
        ? new AssignedUserName.fromJson(json['salutation'])
        : null;
    firstName = json['first_name'] != null
        ? new AssignedUserName.fromJson(json['first_name'])
        : null;
    lastName = json['last_name'] != null
        ? new AssignedUserName.fromJson(json['last_name'])
        : null;
    fullName = json['full_name'] != null
        ? new AssignedUserName.fromJson(json['full_name'])
        : null;
    title = json['title'] != null
        ? new AssignedUserName.fromJson(json['title'])
        : null;
    photo = json['photo'] != null
        ? new AssignedUserName.fromJson(json['photo'])
        : null;
    department = json['department'] != null
        ? new AssignedUserName.fromJson(json['department'])
        : null;
    doNotCall = json['do_not_call'] != null
        ? new AssignedUserName.fromJson(json['do_not_call'])
        : null;
    phoneHome = json['phone_home'] != null
        ? new AssignedUserName.fromJson(json['phone_home'])
        : null;
    email = json['email'] != null
        ? new AssignedUserName.fromJson(json['email'])
        : null;
    phoneMobile = json['phone_mobile'] != null
        ? new AssignedUserName.fromJson(json['phone_mobile'])
        : null;
    phoneWork = json['phone_work'] != null
        ? new AssignedUserName.fromJson(json['phone_work'])
        : null;
    phoneOther = json['phone_other'] != null
        ? new AssignedUserName.fromJson(json['phone_other'])
        : null;
    phoneFax = json['phone_fax'] != null
        ? new AssignedUserName.fromJson(json['phone_fax'])
        : null;
    email1 = json['email1'] != null
        ? new AssignedUserName.fromJson(json['email1'])
        : null;
    email2 = json['email2'] != null
        ? new AssignedUserName.fromJson(json['email2'])
        : null;
    invalidEmail = json['invalid_email'] != null
        ? new AssignedUserName.fromJson(json['invalid_email'])
        : null;
    emailOptOut = json['email_opt_out'] != null
        ? new AssignedUserName.fromJson(json['email_opt_out'])
        : null;
    lawfulBasis = json['lawful_basis'] != null
        ? new AssignedUserName.fromJson(json['lawful_basis'])
        : null;
    dateReviewed = json['date_reviewed'] != null
        ? new AssignedUserName.fromJson(json['date_reviewed'])
        : null;
    lawfulBasisSource = json['lawful_basis_source'] != null
        ? new AssignedUserName.fromJson(json['lawful_basis_source'])
        : null;
    primaryAddressStreet = json['primary_address_street'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street'])
        : null;
    primaryAddressStreet2 = json['primary_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street_2'])
        : null;
    primaryAddressStreet3 = json['primary_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['primary_address_street_3'])
        : null;
    primaryAddressCity = json['primary_address_city'] != null
        ? new AssignedUserName.fromJson(json['primary_address_city'])
        : null;
    primaryAddressState = json['primary_address_state'] != null
        ? new AssignedUserName.fromJson(json['primary_address_state'])
        : null;
    primaryAddressPostalcode = json['primary_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['primary_address_postalcode'])
        : null;
    primaryAddressCountry = json['primary_address_country'] != null
        ? new AssignedUserName.fromJson(json['primary_address_country'])
        : null;
    altAddressStreet = json['alt_address_street'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street'])
        : null;
    altAddressStreet2 = json['alt_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street_2'])
        : null;
    altAddressStreet3 = json['alt_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['alt_address_street_3'])
        : null;
    altAddressCity = json['alt_address_city'] != null
        ? new AssignedUserName.fromJson(json['alt_address_city'])
        : null;
    altAddressState = json['alt_address_state'] != null
        ? new AssignedUserName.fromJson(json['alt_address_state'])
        : null;
    altAddressPostalcode = json['alt_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['alt_address_postalcode'])
        : null;
    altAddressCountry = json['alt_address_country'] != null
        ? new AssignedUserName.fromJson(json['alt_address_country'])
        : null;
    assistant = json['assistant'] != null
        ? new AssignedUserName.fromJson(json['assistant'])
        : null;
    assistantPhone = json['assistant_phone'] != null
        ? new AssignedUserName.fromJson(json['assistant_phone'])
        : null;
    emailAddressesNonPrimary = json['email_addresses_non_primary'] != null
        ? new AssignedUserName.fromJson(json['email_addresses_non_primary'])
        : null;
    emailAndName1 = json['email_and_name1'] != null
        ? new AssignedUserName.fromJson(json['email_and_name1'])
        : null;
    leadSource = json['lead_source'] != null
        ? new AssignedUserName.fromJson(json['lead_source'])
        : null;
    opportunityRoleFields = json['opportunity_role_fields'] != null
        ? new AssignedUserName.fromJson(json['opportunity_role_fields'])
        : null;
    opportunityRoleId = json['opportunity_role_id'] != null
        ? new AssignedUserName.fromJson(json['opportunity_role_id'])
        : null;
    opportunityRole = json['opportunity_role'] != null
        ? new AssignedUserName.fromJson(json['opportunity_role'])
        : null;
    reportsToId = json['reports_to_id'] != null
        ? new AssignedUserName.fromJson(json['reports_to_id'])
        : null;
    reportToName = json['report_to_name'] != null
        ? new AssignedUserName.fromJson(json['report_to_name'])
        : null;
    birthdate = json['birthdate'] != null
        ? new AssignedUserName.fromJson(json['birthdate'])
        : null;
    campaignId = json['campaign_id'] != null
        ? new AssignedUserName.fromJson(json['campaign_id'])
        : null;
    campaignName = json['campaign_name'] != null
        ? new AssignedUserName.fromJson(json['campaign_name'])
        : null;
    cAcceptStatusFields = json['c_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['c_accept_status_fields'])
        : null;
    mAcceptStatusFields = json['m_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['m_accept_status_fields'])
        : null;
    acceptStatusId = json['accept_status_id'] != null
        ? new AssignedUserName.fromJson(json['accept_status_id'])
        : null;
    acceptStatusName = json['accept_status_name'] != null
        ? new AssignedUserName.fromJson(json['accept_status_name'])
        : null;
    syncContact = json['sync_contact'] != null
        ? new AssignedUserName.fromJson(json['sync_contact'])
        : null;
    eInviteStatusFields = json['e_invite_status_fields'] != null
        ? new AssignedUserName.fromJson(json['e_invite_status_fields'])
        : null;
    eventStatusName = json['event_status_name'] != null
        ? new AssignedUserName.fromJson(json['event_status_name'])
        : null;
    eventInviteId = json['event_invite_id'] != null
        ? new AssignedUserName.fromJson(json['event_invite_id'])
        : null;
    eAcceptStatusFields = json['e_accept_status_fields'] != null
        ? new AssignedUserName.fromJson(json['e_accept_status_fields'])
        : null;
    eventAcceptStatus = json['event_accept_status'] != null
        ? new AssignedUserName.fromJson(json['event_accept_status'])
        : null;
    eventStatusId = json['event_status_id'] != null
        ? new AssignedUserName.fromJson(json['event_status_id'])
        : null;
    joomlaAccountId = json['joomla_account_id'] != null
        ? new AssignedUserName.fromJson(json['joomla_account_id'])
        : null;
    portalAccountDisabled = json['portal_account_disabled'] != null
        ? new AssignedUserName.fromJson(json['portal_account_disabled'])
        : null;
    joomlaAccountAccess = json['joomla_account_access'] != null
        ? new AssignedUserName.fromJson(json['joomla_account_access'])
        : null;
    portalUserType = json['portal_user_type'] != null
        ? new AssignedUserName.fromJson(json['portal_user_type'])
        : null;
    isYourOwnAccountC = json['is_your_own_account_c'] != null
        ? new AssignedUserName.fromJson(json['is_your_own_account_c'])
        : null;
    jjwgMapsLngC = json['jjwg_maps_lng_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lng_c'])
        : null;
    jjwgMapsGeocodeStatusC = json['jjwg_maps_geocode_status_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_geocode_status_c'])
        : null;
    profilePictureC = json['profile_picture_c'] != null
        ? new AssignedUserName.fromJson(json['profile_picture_c'])
        : null;
    birthDateC = json['birth_date_c'] != null
        ? new AssignedUserName.fromJson(json['birth_date_c'])
        : null;
    jjwgMapsAddressC = json['jjwg_maps_address_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_address_c'])
        : null;
    jjwgMapsLatC = json['jjwg_maps_lat_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lat_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.salutation != null) {
      data['salutation'] = this.salutation.toJson();
    }
    if (this.firstName != null) {
      data['first_name'] = this.firstName.toJson();
    }
    if (this.lastName != null) {
      data['last_name'] = this.lastName.toJson();
    }
    if (this.fullName != null) {
      data['full_name'] = this.fullName.toJson();
    }
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    if (this.photo != null) {
      data['photo'] = this.photo.toJson();
    }
    if (this.department != null) {
      data['department'] = this.department.toJson();
    }
    if (this.doNotCall != null) {
      data['do_not_call'] = this.doNotCall.toJson();
    }
    if (this.phoneHome != null) {
      data['phone_home'] = this.phoneHome.toJson();
    }
    if (this.email != null) {
      data['email'] = this.email.toJson();
    }
    if (this.phoneMobile != null) {
      data['phone_mobile'] = this.phoneMobile.toJson();
    }
    if (this.phoneWork != null) {
      data['phone_work'] = this.phoneWork.toJson();
    }
    if (this.phoneOther != null) {
      data['phone_other'] = this.phoneOther.toJson();
    }
    if (this.phoneFax != null) {
      data['phone_fax'] = this.phoneFax.toJson();
    }
    if (this.email1 != null) {
      data['email1'] = this.email1.toJson();
    }
    if (this.email2 != null) {
      data['email2'] = this.email2.toJson();
    }
    if (this.invalidEmail != null) {
      data['invalid_email'] = this.invalidEmail.toJson();
    }
    if (this.emailOptOut != null) {
      data['email_opt_out'] = this.emailOptOut.toJson();
    }
    if (this.lawfulBasis != null) {
      data['lawful_basis'] = this.lawfulBasis.toJson();
    }
    if (this.dateReviewed != null) {
      data['date_reviewed'] = this.dateReviewed.toJson();
    }
    if (this.lawfulBasisSource != null) {
      data['lawful_basis_source'] = this.lawfulBasisSource.toJson();
    }
    if (this.primaryAddressStreet != null) {
      data['primary_address_street'] = this.primaryAddressStreet.toJson();
    }
    if (this.primaryAddressStreet2 != null) {
      data['primary_address_street_2'] = this.primaryAddressStreet2.toJson();
    }
    if (this.primaryAddressStreet3 != null) {
      data['primary_address_street_3'] = this.primaryAddressStreet3.toJson();
    }
    if (this.primaryAddressCity != null) {
      data['primary_address_city'] = this.primaryAddressCity.toJson();
    }
    if (this.primaryAddressState != null) {
      data['primary_address_state'] = this.primaryAddressState.toJson();
    }
    if (this.primaryAddressPostalcode != null) {
      data['primary_address_postalcode'] =
          this.primaryAddressPostalcode.toJson();
    }
    if (this.primaryAddressCountry != null) {
      data['primary_address_country'] = this.primaryAddressCountry.toJson();
    }
    if (this.altAddressStreet != null) {
      data['alt_address_street'] = this.altAddressStreet.toJson();
    }
    if (this.altAddressStreet2 != null) {
      data['alt_address_street_2'] = this.altAddressStreet2.toJson();
    }
    if (this.altAddressStreet3 != null) {
      data['alt_address_street_3'] = this.altAddressStreet3.toJson();
    }
    if (this.altAddressCity != null) {
      data['alt_address_city'] = this.altAddressCity.toJson();
    }
    if (this.altAddressState != null) {
      data['alt_address_state'] = this.altAddressState.toJson();
    }
    if (this.altAddressPostalcode != null) {
      data['alt_address_postalcode'] = this.altAddressPostalcode.toJson();
    }
    if (this.altAddressCountry != null) {
      data['alt_address_country'] = this.altAddressCountry.toJson();
    }
    if (this.assistant != null) {
      data['assistant'] = this.assistant.toJson();
    }
    if (this.assistantPhone != null) {
      data['assistant_phone'] = this.assistantPhone.toJson();
    }
    if (this.emailAddressesNonPrimary != null) {
      data['email_addresses_non_primary'] =
          this.emailAddressesNonPrimary.toJson();
    }
    if (this.emailAndName1 != null) {
      data['email_and_name1'] = this.emailAndName1.toJson();
    }
    if (this.leadSource != null) {
      data['lead_source'] = this.leadSource.toJson();
    }
    if (this.opportunityRoleFields != null) {
      data['opportunity_role_fields'] = this.opportunityRoleFields.toJson();
    }
    if (this.opportunityRoleId != null) {
      data['opportunity_role_id'] = this.opportunityRoleId.toJson();
    }
    if (this.opportunityRole != null) {
      data['opportunity_role'] = this.opportunityRole.toJson();
    }
    if (this.reportsToId != null) {
      data['reports_to_id'] = this.reportsToId.toJson();
    }
    if (this.reportToName != null) {
      data['report_to_name'] = this.reportToName.toJson();
    }
    if (this.birthdate != null) {
      data['birthdate'] = this.birthdate.toJson();
    }
    if (this.campaignId != null) {
      data['campaign_id'] = this.campaignId.toJson();
    }
    if (this.campaignName != null) {
      data['campaign_name'] = this.campaignName.toJson();
    }
    if (this.cAcceptStatusFields != null) {
      data['c_accept_status_fields'] = this.cAcceptStatusFields.toJson();
    }
    if (this.mAcceptStatusFields != null) {
      data['m_accept_status_fields'] = this.mAcceptStatusFields.toJson();
    }
    if (this.acceptStatusId != null) {
      data['accept_status_id'] = this.acceptStatusId.toJson();
    }
    if (this.acceptStatusName != null) {
      data['accept_status_name'] = this.acceptStatusName.toJson();
    }
    if (this.syncContact != null) {
      data['sync_contact'] = this.syncContact.toJson();
    }
    if (this.eInviteStatusFields != null) {
      data['e_invite_status_fields'] = this.eInviteStatusFields.toJson();
    }
    if (this.eventStatusName != null) {
      data['event_status_name'] = this.eventStatusName.toJson();
    }
    if (this.eventInviteId != null) {
      data['event_invite_id'] = this.eventInviteId.toJson();
    }
    if (this.eAcceptStatusFields != null) {
      data['e_accept_status_fields'] = this.eAcceptStatusFields.toJson();
    }
    if (this.eventAcceptStatus != null) {
      data['event_accept_status'] = this.eventAcceptStatus.toJson();
    }
    if (this.eventStatusId != null) {
      data['event_status_id'] = this.eventStatusId.toJson();
    }
    if (this.joomlaAccountId != null) {
      data['joomla_account_id'] = this.joomlaAccountId.toJson();
    }
    if (this.portalAccountDisabled != null) {
      data['portal_account_disabled'] = this.portalAccountDisabled.toJson();
    }
    if (this.joomlaAccountAccess != null) {
      data['joomla_account_access'] = this.joomlaAccountAccess.toJson();
    }
    if (this.portalUserType != null) {
      data['portal_user_type'] = this.portalUserType.toJson();
    }
    if (this.isYourOwnAccountC != null) {
      data['is_your_own_account_c'] = this.isYourOwnAccountC.toJson();
    }
    if (this.jjwgMapsLngC != null) {
      data['jjwg_maps_lng_c'] = this.jjwgMapsLngC.toJson();
    }
    if (this.jjwgMapsGeocodeStatusC != null) {
      data['jjwg_maps_geocode_status_c'] = this.jjwgMapsGeocodeStatusC.toJson();
    }
    if (this.profilePictureC != null) {
      data['profile_picture_c'] = this.profilePictureC.toJson();
    }
    if (this.birthDateC != null) {
      data['birth_date_c'] = this.birthDateC.toJson();
    }
    if (this.jjwgMapsAddressC != null) {
      data['jjwg_maps_address_c'] = this.jjwgMapsAddressC.toJson();
    }
    if (this.jjwgMapsLatC != null) {
      data['jjwg_maps_lat_c'] = this.jjwgMapsLatC.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
