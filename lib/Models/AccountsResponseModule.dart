class AccountsResponseModule {
  int resultCount;
  String totalCount;
  int nextOffset;
  List<AEntryList> entryList;
 // List<Null> relationshipList;

  AccountsResponseModule(
      {this.resultCount,
        this.totalCount,
        this.nextOffset,
        this.entryList,
        // this.relationshipList
      });

  AccountsResponseModule.fromJson(Map<String, dynamic> json) {
    resultCount = json['result_count'];
    totalCount = json['total_count'];
    nextOffset = json['next_offset'];
    if (json['entry_list'] != null) {
      entryList = new List<AEntryList>();
      json['entry_list'].forEach((v) {
        entryList.add(new AEntryList.fromJson(v));
      });
    }
    // if (json['relationship_list'] != null) {
    //   relationshipList = new List<Null>();
    //   json['relationship_list'].forEach((v) {
    //     relationshipList.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result_count'] = this.resultCount;
    data['total_count'] = this.totalCount;
    data['next_offset'] = this.nextOffset;
    if (this.entryList != null) {
      data['entry_list'] = this.entryList.map((v) => v.toJson()).toList();
    }
    // if (this.relationshipList != null) {
    //   data['relationship_list'] =
    //       this.relationshipList.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

class AEntryList {
  String id;
  String moduleName;
  NameValueList nameValueList;

  AEntryList({this.id, this.moduleName, this.nameValueList});

  AEntryList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduleName = json['module_name'];
    nameValueList = json['name_value_list'] != null
        ? new NameValueList.fromJson(json['name_value_list'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['module_name'] = this.moduleName;
    if (this.nameValueList != null) {
      data['name_value_list'] = this.nameValueList.toJson();
    }
    return data;
  }
}

class NameValueList {
  AssignedUserName assignedUserName;
  AssignedUserName modifiedByName;
  AssignedUserName createdByName;
  AssignedUserName id;
  AssignedUserName name;
  AssignedUserName dateEntered;
  AssignedUserName dateModified;
  AssignedUserName modifiedUserId;
  AssignedUserName createdBy;
  AssignedUserName description;
  AssignedUserName deleted;
  AssignedUserName assignedUserId;
  AssignedUserName accountType;
  AssignedUserName industry;
  AssignedUserName annualRevenue;
  AssignedUserName phoneFax;
  AssignedUserName billingAddressStreet;
  AssignedUserName billingAddressStreet2;
  AssignedUserName billingAddressStreet3;
  AssignedUserName billingAddressStreet4;
  AssignedUserName billingAddressCity;
  AssignedUserName billingAddressState;
  AssignedUserName billingAddressPostalcode;
  AssignedUserName billingAddressCountry;
  AssignedUserName rating;
  AssignedUserName phoneOffice;
  AssignedUserName phoneAlternate;
  AssignedUserName website;
  AssignedUserName ownership;
  AssignedUserName employees;
  AssignedUserName tickerSymbol;
  AssignedUserName shippingAddressStreet;
  AssignedUserName shippingAddressStreet2;
  AssignedUserName shippingAddressStreet3;
  AssignedUserName shippingAddressStreet4;
  AssignedUserName shippingAddressCity;
  AssignedUserName shippingAddressState;
  AssignedUserName shippingAddressPostalcode;
  AssignedUserName shippingAddressCountry;
  AssignedUserName email1;
  AssignedUserName emailAddressesNonPrimary;
  AssignedUserName parentId;
  AssignedUserName sicCode;
  AssignedUserName parentName;
  AssignedUserName emailOptOut;
  AssignedUserName invalidEmail;
  AssignedUserName email;
  AssignedUserName campaignId;
  AssignedUserName campaignName;
  AssignedUserName clientNameC;
  AssignedUserName jjwgMapsLngC;
  AssignedUserName jjwgMapsGeocodeStatusC;
  AssignedUserName statusC;
  AssignedUserName accountInformationC;
  AssignedUserName jjwgMapsAddressC;
  AssignedUserName jjwgMapsLatC;

  NameValueList(
      {this.assignedUserName,
        this.modifiedByName,
        this.createdByName,
        this.id,
        this.name,
        this.dateEntered,
        this.dateModified,
        this.modifiedUserId,
        this.createdBy,
        this.description,
        this.deleted,
        this.assignedUserId,
        this.accountType,
        this.industry,
        this.annualRevenue,
        this.phoneFax,
        this.billingAddressStreet,
        this.billingAddressStreet2,
        this.billingAddressStreet3,
        this.billingAddressStreet4,
        this.billingAddressCity,
        this.billingAddressState,
        this.billingAddressPostalcode,
        this.billingAddressCountry,
        this.rating,
        this.phoneOffice,
        this.phoneAlternate,
        this.website,
        this.ownership,
        this.employees,
        this.tickerSymbol,
        this.shippingAddressStreet,
        this.shippingAddressStreet2,
        this.shippingAddressStreet3,
        this.shippingAddressStreet4,
        this.shippingAddressCity,
        this.shippingAddressState,
        this.shippingAddressPostalcode,
        this.shippingAddressCountry,
        this.email1,
        this.emailAddressesNonPrimary,
        this.parentId,
        this.sicCode,
        this.parentName,
        this.emailOptOut,
        this.invalidEmail,
        this.email,
        this.campaignId,
        this.campaignName,
        this.clientNameC,
        this.jjwgMapsLngC,
        this.jjwgMapsGeocodeStatusC,
        this.statusC,
        this.accountInformationC,
        this.jjwgMapsAddressC,
        this.jjwgMapsLatC});

  NameValueList.fromJson(Map<String, dynamic> json) {
    assignedUserName = json['assigned_user_name'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_name'])
        : null;
    modifiedByName = json['modified_by_name'] != null
        ? new AssignedUserName.fromJson(json['modified_by_name'])
        : null;
    createdByName = json['created_by_name'] != null
        ? new AssignedUserName.fromJson(json['created_by_name'])
        : null;
    id = json['id'] != null ? new AssignedUserName.fromJson(json['id']) : null;
    name = json['name'] != null
        ? new AssignedUserName.fromJson(json['name'])
        : null;
    dateEntered = json['date_entered'] != null
        ? new AssignedUserName.fromJson(json['date_entered'])
        : null;
    dateModified = json['date_modified'] != null
        ? new AssignedUserName.fromJson(json['date_modified'])
        : null;
    modifiedUserId = json['modified_user_id'] != null
        ? new AssignedUserName.fromJson(json['modified_user_id'])
        : null;
    createdBy = json['created_by'] != null
        ? new AssignedUserName.fromJson(json['created_by'])
        : null;
    description = json['description'] != null
        ? new AssignedUserName.fromJson(json['description'])
        : null;
    deleted = json['deleted'] != null
        ? new AssignedUserName.fromJson(json['deleted'])
        : null;
    assignedUserId = json['assigned_user_id'] != null
        ? new AssignedUserName.fromJson(json['assigned_user_id'])
        : null;
    accountType = json['account_type'] != null
        ? new AssignedUserName.fromJson(json['account_type'])
        : null;
    industry = json['industry'] != null
        ? new AssignedUserName.fromJson(json['industry'])
        : null;
    annualRevenue = json['annual_revenue'] != null
        ? new AssignedUserName.fromJson(json['annual_revenue'])
        : null;
    phoneFax = json['phone_fax'] != null
        ? new AssignedUserName.fromJson(json['phone_fax'])
        : null;
    billingAddressStreet = json['billing_address_street'] != null
        ? new AssignedUserName.fromJson(json['billing_address_street'])
        : null;
    billingAddressStreet2 = json['billing_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['billing_address_street_2'])
        : null;
    billingAddressStreet3 = json['billing_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['billing_address_street_3'])
        : null;
    billingAddressStreet4 = json['billing_address_street_4'] != null
        ? new AssignedUserName.fromJson(json['billing_address_street_4'])
        : null;
    billingAddressCity = json['billing_address_city'] != null
        ? new AssignedUserName.fromJson(json['billing_address_city'])
        : null;
    billingAddressState = json['billing_address_state'] != null
        ? new AssignedUserName.fromJson(json['billing_address_state'])
        : null;
    billingAddressPostalcode = json['billing_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['billing_address_postalcode'])
        : null;
    billingAddressCountry = json['billing_address_country'] != null
        ? new AssignedUserName.fromJson(json['billing_address_country'])
        : null;
    rating = json['rating'] != null
        ? new AssignedUserName.fromJson(json['rating'])
        : null;
    phoneOffice = json['phone_office'] != null
        ? new AssignedUserName.fromJson(json['phone_office'])
        : null;
    phoneAlternate = json['phone_alternate'] != null
        ? new AssignedUserName.fromJson(json['phone_alternate'])
        : null;
    website = json['website'] != null
        ? new AssignedUserName.fromJson(json['website'])
        : null;
    ownership = json['ownership'] != null
        ? new AssignedUserName.fromJson(json['ownership'])
        : null;
    employees = json['employees'] != null
        ? new AssignedUserName.fromJson(json['employees'])
        : null;
    tickerSymbol = json['ticker_symbol'] != null
        ? new AssignedUserName.fromJson(json['ticker_symbol'])
        : null;
    shippingAddressStreet = json['shipping_address_street'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_street'])
        : null;
    shippingAddressStreet2 = json['shipping_address_street_2'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_street_2'])
        : null;
    shippingAddressStreet3 = json['shipping_address_street_3'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_street_3'])
        : null;
    shippingAddressStreet4 = json['shipping_address_street_4'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_street_4'])
        : null;
    shippingAddressCity = json['shipping_address_city'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_city'])
        : null;
    shippingAddressState = json['shipping_address_state'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_state'])
        : null;
    shippingAddressPostalcode = json['shipping_address_postalcode'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_postalcode'])
        : null;
    shippingAddressCountry = json['shipping_address_country'] != null
        ? new AssignedUserName.fromJson(json['shipping_address_country'])
        : null;
    email1 = json['email1'] != null
        ? new AssignedUserName.fromJson(json['email1'])
        : null;
    emailAddressesNonPrimary = json['email_addresses_non_primary'] != null
        ? new AssignedUserName.fromJson(json['email_addresses_non_primary'])
        : null;
    parentId = json['parent_id'] != null
        ? new AssignedUserName.fromJson(json['parent_id'])
        : null;
    sicCode = json['sic_code'] != null
        ? new AssignedUserName.fromJson(json['sic_code'])
        : null;
    parentName = json['parent_name'] != null
        ? new AssignedUserName.fromJson(json['parent_name'])
        : null;
    emailOptOut = json['email_opt_out'] != null
        ? new AssignedUserName.fromJson(json['email_opt_out'])
        : null;
    invalidEmail = json['invalid_email'] != null
        ? new AssignedUserName.fromJson(json['invalid_email'])
        : null;
    email = json['email'] != null
        ? new AssignedUserName.fromJson(json['email'])
        : null;
    campaignId = json['campaign_id'] != null
        ? new AssignedUserName.fromJson(json['campaign_id'])
        : null;
    campaignName = json['campaign_name'] != null
        ? new AssignedUserName.fromJson(json['campaign_name'])
        : null;
    clientNameC = json['client_name_c'] != null
        ? new AssignedUserName.fromJson(json['client_name_c'])
        : null;
    jjwgMapsLngC = json['jjwg_maps_lng_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lng_c'])
        : null;
    jjwgMapsGeocodeStatusC = json['jjwg_maps_geocode_status_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_geocode_status_c'])
        : null;
    statusC = json['status_c'] != null
        ? new AssignedUserName.fromJson(json['status_c'])
        : null;
    accountInformationC = json['account_information_c'] != null
        ? new AssignedUserName.fromJson(json['account_information_c'])
        : null;
    jjwgMapsAddressC = json['jjwg_maps_address_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_address_c'])
        : null;
    jjwgMapsLatC = json['jjwg_maps_lat_c'] != null
        ? new AssignedUserName.fromJson(json['jjwg_maps_lat_c'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.assignedUserName != null) {
      data['assigned_user_name'] = this.assignedUserName.toJson();
    }
    if (this.modifiedByName != null) {
      data['modified_by_name'] = this.modifiedByName.toJson();
    }
    if (this.createdByName != null) {
      data['created_by_name'] = this.createdByName.toJson();
    }
    if (this.id != null) {
      data['id'] = this.id.toJson();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.dateEntered != null) {
      data['date_entered'] = this.dateEntered.toJson();
    }
    if (this.dateModified != null) {
      data['date_modified'] = this.dateModified.toJson();
    }
    if (this.modifiedUserId != null) {
      data['modified_user_id'] = this.modifiedUserId.toJson();
    }
    if (this.createdBy != null) {
      data['created_by'] = this.createdBy.toJson();
    }
    if (this.description != null) {
      data['description'] = this.description.toJson();
    }
    if (this.deleted != null) {
      data['deleted'] = this.deleted.toJson();
    }
    if (this.assignedUserId != null) {
      data['assigned_user_id'] = this.assignedUserId.toJson();
    }
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    if (this.industry != null) {
      data['industry'] = this.industry.toJson();
    }
    if (this.annualRevenue != null) {
      data['annual_revenue'] = this.annualRevenue.toJson();
    }
    if (this.phoneFax != null) {
      data['phone_fax'] = this.phoneFax.toJson();
    }
    if (this.billingAddressStreet != null) {
      data['billing_address_street'] = this.billingAddressStreet.toJson();
    }
    if (this.billingAddressStreet2 != null) {
      data['billing_address_street_2'] = this.billingAddressStreet2.toJson();
    }
    if (this.billingAddressStreet3 != null) {
      data['billing_address_street_3'] = this.billingAddressStreet3.toJson();
    }
    if (this.billingAddressStreet4 != null) {
      data['billing_address_street_4'] = this.billingAddressStreet4.toJson();
    }
    if (this.billingAddressCity != null) {
      data['billing_address_city'] = this.billingAddressCity.toJson();
    }
    if (this.billingAddressState != null) {
      data['billing_address_state'] = this.billingAddressState.toJson();
    }
    if (this.billingAddressPostalcode != null) {
      data['billing_address_postalcode'] =
          this.billingAddressPostalcode.toJson();
    }
    if (this.billingAddressCountry != null) {
      data['billing_address_country'] = this.billingAddressCountry.toJson();
    }
    if (this.rating != null) {
      data['rating'] = this.rating.toJson();
    }
    if (this.phoneOffice != null) {
      data['phone_office'] = this.phoneOffice.toJson();
    }
    if (this.phoneAlternate != null) {
      data['phone_alternate'] = this.phoneAlternate.toJson();
    }
    if (this.website != null) {
      data['website'] = this.website.toJson();
    }
    if (this.ownership != null) {
      data['ownership'] = this.ownership.toJson();
    }
    if (this.employees != null) {
      data['employees'] = this.employees.toJson();
    }
    if (this.tickerSymbol != null) {
      data['ticker_symbol'] = this.tickerSymbol.toJson();
    }
    if (this.shippingAddressStreet != null) {
      data['shipping_address_street'] = this.shippingAddressStreet.toJson();
    }
    if (this.shippingAddressStreet2 != null) {
      data['shipping_address_street_2'] = this.shippingAddressStreet2.toJson();
    }
    if (this.shippingAddressStreet3 != null) {
      data['shipping_address_street_3'] = this.shippingAddressStreet3.toJson();
    }
    if (this.shippingAddressStreet4 != null) {
      data['shipping_address_street_4'] = this.shippingAddressStreet4.toJson();
    }
    if (this.shippingAddressCity != null) {
      data['shipping_address_city'] = this.shippingAddressCity.toJson();
    }
    if (this.shippingAddressState != null) {
      data['shipping_address_state'] = this.shippingAddressState.toJson();
    }
    if (this.shippingAddressPostalcode != null) {
      data['shipping_address_postalcode'] =
          this.shippingAddressPostalcode.toJson();
    }
    if (this.shippingAddressCountry != null) {
      data['shipping_address_country'] = this.shippingAddressCountry.toJson();
    }
    if (this.email1 != null) {
      data['email1'] = this.email1.toJson();
    }
    if (this.emailAddressesNonPrimary != null) {
      data['email_addresses_non_primary'] =
          this.emailAddressesNonPrimary.toJson();
    }
    if (this.parentId != null) {
      data['parent_id'] = this.parentId.toJson();
    }
    if (this.sicCode != null) {
      data['sic_code'] = this.sicCode.toJson();
    }
    if (this.parentName != null) {
      data['parent_name'] = this.parentName.toJson();
    }
    if (this.emailOptOut != null) {
      data['email_opt_out'] = this.emailOptOut.toJson();
    }
    if (this.invalidEmail != null) {
      data['invalid_email'] = this.invalidEmail.toJson();
    }
    if (this.email != null) {
      data['email'] = this.email.toJson();
    }
    if (this.campaignId != null) {
      data['campaign_id'] = this.campaignId.toJson();
    }
    if (this.campaignName != null) {
      data['campaign_name'] = this.campaignName.toJson();
    }
    if (this.clientNameC != null) {
      data['client_name_c'] = this.clientNameC.toJson();
    }
    if (this.jjwgMapsLngC != null) {
      data['jjwg_maps_lng_c'] = this.jjwgMapsLngC.toJson();
    }
    if (this.jjwgMapsGeocodeStatusC != null) {
      data['jjwg_maps_geocode_status_c'] = this.jjwgMapsGeocodeStatusC.toJson();
    }
    if (this.statusC != null) {
      data['status_c'] = this.statusC.toJson();
    }
    if (this.accountInformationC != null) {
      data['account_information_c'] = this.accountInformationC.toJson();
    }
    if (this.jjwgMapsAddressC != null) {
      data['jjwg_maps_address_c'] = this.jjwgMapsAddressC.toJson();
    }
    if (this.jjwgMapsLatC != null) {
      data['jjwg_maps_lat_c'] = this.jjwgMapsLatC.toJson();
    }
    return data;
  }
}

class AssignedUserName {
  String name;
  String value;

  AssignedUserName({this.name, this.value});

  AssignedUserName.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
