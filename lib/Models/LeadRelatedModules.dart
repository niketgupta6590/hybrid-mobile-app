class LeadRelatedModulesResponse {
  String moduleName;
  String tableName;
  LinkFields linkFields;

  LeadRelatedModulesResponse(
      {this.moduleName, this.tableName, this.linkFields});

  LeadRelatedModulesResponse.fromJson(Map<String, dynamic> json) {
    moduleName = json['module_name'];
    tableName = json['table_name'];
    linkFields = json['link_fields'] != null
        ? new LinkFields.fromJson(json['link_fields'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module_name'] = this.moduleName;
    data['table_name'] = this.tableName;
    if (this.linkFields != null) {
      data['link_fields'] = this.linkFields.toJson();
    }
    return data;
  }
}

class LinkFields {
  CreatedByLink createdByLink;
  CreatedByLink modifiedUserLink;
  CreatedByLink assignedUserLink;
  CreatedByLink securityGroups;
  CreatedByLink emailAddressesPrimary;
  CreatedByLink emailAddresses;
  CreatedByLink reportsToLink;
  CreatedByLink reportees;
  CreatedByLink contacts;
  CreatedByLink accounts;
  CreatedByLink contact;
  CreatedByLink opportunity;
  CreatedByLink campaignLeads;
  CreatedByLink tasks;
  CreatedByLink notes;
  CreatedByLink meetings;
  CreatedByLink calls;
  CreatedByLink oldmeetings;
  CreatedByLink oldcalls;
  CreatedByLink emails;
  CreatedByLink campaigns;
  CreatedByLink prospectLists;
  CreatedByLink fpEventsLeads1;
  CreatedByLink leadsDocuments1;
  CreatedByLink leadsAbc12Property1;

  LinkFields(
      {this.createdByLink,
        this.modifiedUserLink,
        this.assignedUserLink,
        this.securityGroups,
        this.emailAddressesPrimary,
        this.emailAddresses,
        this.reportsToLink,
        this.reportees,
        this.contacts,
        this.accounts,
        this.contact,
        this.opportunity,
        this.campaignLeads,
        this.tasks,
        this.notes,
        this.meetings,
        this.calls,
        this.oldmeetings,
        this.oldcalls,
        this.emails,
        this.campaigns,
        this.prospectLists,
        this.fpEventsLeads1,
        this.leadsDocuments1,
        this.leadsAbc12Property1});

  LinkFields.fromJson(Map<String, dynamic> json) {
    createdByLink = json['created_by_link'] != null
        ? new CreatedByLink.fromJson(json['created_by_link'])
        : null;
    modifiedUserLink = json['modified_user_link'] != null
        ? new CreatedByLink.fromJson(json['modified_user_link'])
        : null;
    assignedUserLink = json['assigned_user_link'] != null
        ? new CreatedByLink.fromJson(json['assigned_user_link'])
        : null;
    securityGroups = json['SecurityGroups'] != null
        ? new CreatedByLink.fromJson(json['SecurityGroups'])
        : null;
    emailAddressesPrimary = json['email_addresses_primary'] != null
        ? new CreatedByLink.fromJson(json['email_addresses_primary'])
        : null;
    emailAddresses = json['email_addresses'] != null
        ? new CreatedByLink.fromJson(json['email_addresses'])
        : null;
    reportsToLink = json['reports_to_link'] != null
        ? new CreatedByLink.fromJson(json['reports_to_link'])
        : null;
    reportees = json['reportees'] != null
        ? new CreatedByLink.fromJson(json['reportees'])
        : null;
    contacts = json['contacts'] != null
        ? new CreatedByLink.fromJson(json['contacts'])
        : null;
    accounts = json['accounts'] != null
        ? new CreatedByLink.fromJson(json['accounts'])
        : null;
    contact = json['contact'] != null
        ? new CreatedByLink.fromJson(json['contact'])
        : null;
    opportunity = json['opportunity'] != null
        ? new CreatedByLink.fromJson(json['opportunity'])
        : null;
    campaignLeads = json['campaign_leads'] != null
        ? new CreatedByLink.fromJson(json['campaign_leads'])
        : null;
    tasks = json['tasks'] != null
        ? new CreatedByLink.fromJson(json['tasks'])
        : null;
    notes = json['notes'] != null
        ? new CreatedByLink.fromJson(json['notes'])
        : null;
    meetings = json['meetings'] != null
        ? new CreatedByLink.fromJson(json['meetings'])
        : null;
    calls = json['calls'] != null
        ? new CreatedByLink.fromJson(json['calls'])
        : null;
    oldmeetings = json['oldmeetings'] != null
        ? new CreatedByLink.fromJson(json['oldmeetings'])
        : null;
    oldcalls = json['oldcalls'] != null
        ? new CreatedByLink.fromJson(json['oldcalls'])
        : null;
    emails = json['emails'] != null
        ? new CreatedByLink.fromJson(json['emails'])
        : null;
    campaigns = json['campaigns'] != null
        ? new CreatedByLink.fromJson(json['campaigns'])
        : null;
    prospectLists = json['prospect_lists'] != null
        ? new CreatedByLink.fromJson(json['prospect_lists'])
        : null;
    fpEventsLeads1 = json['fp_events_leads_1'] != null
        ? new CreatedByLink.fromJson(json['fp_events_leads_1'])
        : null;
    leadsDocuments1 = json['leads_documents_1'] != null
        ? new CreatedByLink.fromJson(json['leads_documents_1'])
        : null;
    leadsAbc12Property1 = json['leads_abc12_property_1'] != null
        ? new CreatedByLink.fromJson(json['leads_abc12_property_1'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.createdByLink != null) {
      data['created_by_link'] = this.createdByLink.toJson();
    }
    if (this.modifiedUserLink != null) {
      data['modified_user_link'] = this.modifiedUserLink.toJson();
    }
    if (this.assignedUserLink != null) {
      data['assigned_user_link'] = this.assignedUserLink.toJson();
    }
    if (this.securityGroups != null) {
      data['SecurityGroups'] = this.securityGroups.toJson();
    }
    if (this.emailAddressesPrimary != null) {
      data['email_addresses_primary'] = this.emailAddressesPrimary.toJson();
    }
    if (this.emailAddresses != null) {
      data['email_addresses'] = this.emailAddresses.toJson();
    }
    if (this.reportsToLink != null) {
      data['reports_to_link'] = this.reportsToLink.toJson();
    }
    if (this.reportees != null) {
      data['reportees'] = this.reportees.toJson();
    }
    if (this.contacts != null) {
      data['contacts'] = this.contacts.toJson();
    }
    if (this.accounts != null) {
      data['accounts'] = this.accounts.toJson();
    }
    if (this.contact != null) {
      data['contact'] = this.contact.toJson();
    }
    if (this.opportunity != null) {
      data['opportunity'] = this.opportunity.toJson();
    }
    if (this.campaignLeads != null) {
      data['campaign_leads'] = this.campaignLeads.toJson();
    }
    if (this.tasks != null) {
      data['tasks'] = this.tasks.toJson();
    }
    if (this.notes != null) {
      data['notes'] = this.notes.toJson();
    }
    if (this.meetings != null) {
      data['meetings'] = this.meetings.toJson();
    }
    if (this.calls != null) {
      data['calls'] = this.calls.toJson();
    }
    if (this.oldmeetings != null) {
      data['oldmeetings'] = this.oldmeetings.toJson();
    }
    if (this.oldcalls != null) {
      data['oldcalls'] = this.oldcalls.toJson();
    }
    if (this.emails != null) {
      data['emails'] = this.emails.toJson();
    }
    if (this.campaigns != null) {
      data['campaigns'] = this.campaigns.toJson();
    }
    if (this.prospectLists != null) {
      data['prospect_lists'] = this.prospectLists.toJson();
    }
    if (this.fpEventsLeads1 != null) {
      data['fp_events_leads_1'] = this.fpEventsLeads1.toJson();
    }
    if (this.leadsDocuments1 != null) {
      data['leads_documents_1'] = this.leadsDocuments1.toJson();
    }
    if (this.leadsAbc12Property1 != null) {
      data['leads_abc12_property_1'] = this.leadsAbc12Property1.toJson();
    }
    return data;
  }
}

class CreatedByLink {
  String name;
  String type;
  String group;
  String idName;
  String relationship;
  String module;
  String beanName;

  CreatedByLink(
      {this.name,
        this.type,
        this.group,
        this.idName,
        this.relationship,
        this.module,
        this.beanName});

  CreatedByLink.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    group = json['group'];
    idName = json['id_name'];
    relationship = json['relationship'];
    module = json['module'];
    beanName = json['bean_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['group'] = this.group;
    data['id_name'] = this.idName;
    data['relationship'] = this.relationship;
    data['module'] = this.module;
    data['bean_name'] = this.beanName;
    return data;
  }
}
