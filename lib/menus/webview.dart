import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class webview extends StatefulWidget {
  @override
  WebViewLoadUI createState() => WebViewLoadUI();
}

class WebViewLoadUI extends State<webview> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    home: Scaffold (
    body: WebView(
    initialUrl: 'https://dzone.com/articles/flutter-webview-url-listeners',
    javascriptMode: JavascriptMode.unrestricted,

    )
    ),
    );
  }
}
// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:webview_flutter/webview_flutter.dart';
//
//
// class WebviewPage extends StatelessWidget {
//
//   const WebviewPage({Key key}) : super(key: key);
//
//   String selectedUrl = 'http://www.google.com';
//
//   @override
//   Widget build(BuildContext context) {
//   //  InAppWebViewController _webViewController;
//     final Completer<WebViewController> _controller = Completer<WebViewController>();
//
//
//
//
//     @override
//     Widget build(BuildContext context) {
//       return MaterialApp(
//         home: Scaffold(
//           appBar: AppBar(
//             title: const Text('InAppWebView Example'),
//           ),
//           body: WebView(
//             initialUrl: selectedUrl,
//             javascriptMode: JavascriptMode.unrestricted,
//             onWebViewCreated: (WebViewController webViewController) {
//               _controller.complete(webViewController);
//             },
//           )
//         ),
//         // Container(
//         //       child: Column(children: <Widget>[
//         //         Expanded(
//         //           child: InAppWebView(
//         //             initialUrl: "https://flutter.dev/",
//         //             initialOptions: InAppWebViewGroupOptions(
//         //                 crossPlatform: InAppWebViewOptions(
//         //                   debuggingEnabled: true,
//         //                 )
//         //             ),
//         //             onWebViewCreated: (InAppWebViewController controller) {
//         //               _webViewController = controller;
//         //             },
//         //             onLoadStart: (InAppWebViewController controller,
//         //                 String url) {
//         //
//         //             },
//         //             onLoadStop: (InAppWebViewController controller,
//         //                 String url) {
//         //
//         //             },
//         //           ),
//         //         ),
//         //       ])),
//         // ),
//       );
