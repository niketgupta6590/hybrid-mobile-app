import 'package:flutter/material.dart';

class VisibilityExample extends StatefulWidget {

  static  const id = 'visibl';

  @override
  _VisibilityExampleState createState() {
    return _VisibilityExampleState();
  }
}

class _VisibilityExampleState extends State {
  bool _isVisible = true;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visibility Tutorial by Woolha.com',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Visibility Tutorial by Woolha.com'),
          ),
          body: Padding(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RaisedButton(
                  child: Text('Show/Hide Card B'),
                  onPressed: showToast,
                ),
                Card(
                  child: new ListTile(
                    title: Center(
                      child: new Text('A'),
                    ),
                  ),
                ),
                Visibility (
                  visible: _isVisible,
                  child: Card(
                    child: new ListTile(
                      title: Center(
                        child: new Text('B'),
                      ),
                    ),
                  ),
                ),
                Card(
                  child: new ListTile(
                    title: Center(
                      child: new Text('C'),
                    ),
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }
}


// import 'package:flutter/material.dart';
//
// class Setting extends StatefulWidget {
//
//   @override
//   _SettingState createState() => _SettingState();
//
// }
//
// class _SettingState extends State<Setting> {
//   @override
//   Widget build(BuildContext context) {
//     Widget body =
//         Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             ListTile(
//               leading: Icon(Icons.language),
//               title: Text('Language'),
//               trailing: Icon(Icons.arrow_forward_ios),
//               onTap: () {
//
//               },
//             ),
//             ListTile(
//               leading: Icon(Icons.language),
//               title: Text('Language'),
//               trailing: Icon(Icons.arrow_forward_ios),
//               onTap: () {},
//             ),
//           ],
//         );
//
//     return Scaffold(
//
//         body: body
//
//     );
//
//   }
//
//       // MaterialApp(
//       // theme: ThemeData(
//       //   brightness: Brightness.dark,
//       //   primaryColor: Colors.red,
//       // ),
//       // darkTheme: ThemeData(
//       //   brightness: Brightness.dark,
//       // ),
//
//
// }
